from django.db import models
from django.core.urlresolvers import reverse

from snimpy.manager import Manager
from snimpy.manager import load
from snimpy.snmp import SNMPException

class Printer(models.Model):
    STATUS_CHOICES = (
        (1, '1???'),
        (2, 'running'),
        (3, 'warning'),
        (4, '4???'),
        (5, 'down'),
    )
    name = models.CharField(max_length=255, editable=False)
    domain_name = models.CharField(max_length=255, unique=True)
    status = models.PositiveSmallIntegerField(editable=False, choices=STATUS_CHOICES)


    def update_from_snmp(self, save=True):
        self.status = self.manager.hrDeviceStatus[(1)]
        self.name = self.manager.prtGeneralPrinterName[(1)]
        if save:
            self.save()

    def update_printer_information(self):
        try:
            for paper_tray in self.paper_trays.all():
                paper_tray.update_from_snmp()
            for supply in self.supplies.all():
                supply.update_from_snmp()
            self.refresh_alerts() # Since the number of alert is changing we refresh instead of update
            self.update_from_snmp()
        except SNMPException as err:
            print err
    def refresh_paper_trays(self):
        for paper_tray in self.paper_trays.all():
            paper_tray.delete()
        for paper_tray in self.manager.prtInputName:
            paper_tray_object = PaperTray(snmp_id=paper_tray[1], printer=self)
            paper_tray_object.update_from_snmp()

    def refresh_supplies(self):
        for supply in self.supplies.all():
            supply.delete()
        for supply in self.manager.prtMarkerSuppliesDescription:
            supply_object = Supply(snmp_id=supply[1], printer=self)
            supply_object.update_from_snmp()

    def refresh_alerts(self):
        for alert in self.alerts.all():
            alert.delete()
        for alert in self.manager.prtAlertDescription:
            alert_object = Alert(snmp_id=alert[1], printer=self)
            alert_object.update_from_snmp()

    def __init__(self, *args, **kwargs):
        super(Printer, self).__init__(*args, **kwargs)
        if self.pk is not None:
            load("Printer-MIB")
            load("HOST-RESOURCES-MIB")
            self.manager = Manager(self.domain_name, "public", 2)

    def save(self, *args, **kwargs):
        if self.pk is None:
            load("Printer-MIB")
            load("HOST-RESOURCES-MIB")
            self.manager = Manager(self.domain_name, "public", 2)
            self.update_from_snmp(save=False)
            super(Printer, self).save(*args, **kwargs)
            self.refresh_paper_trays()
            self.refresh_supplies()
            self.refresh_alerts()
        else:
            super(Printer, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        """Url vers la view principale d'affichage"""
        return reverse('impressions:printer', args=[str(self.pk)])

class PaperTray(models.Model):
    snmp_id = models.PositiveSmallIntegerField(editable=False)
    name = models.CharField(max_length=30, editable=False)
    printer = models.ForeignKey('Printer', editable=False, related_name='paper_trays')
    #paper_format = models.CharField(max_length=30, editable=False, choices=FORMAT_CHOICES)
    count = models.IntegerField(editable=False)
    max_capacity = models.IntegerField(editable=False)
    dimx = models.IntegerField(editable=False)
    dimy = models.IntegerField(editable=False)


    def update_from_snmp(self):
        self.name = self.printer.manager.prtInputDescription[(1, self.snmp_id)]
        self.count = self.printer.manager.prtInputCurrentLevel[(1, self.snmp_id)]
        self.max_capacity = self.printer.manager.prtInputMaxCapacity[(1, self.snmp_id)]
        self.dimx = self.printer.manager.prtInputMediaDimXFeedDirChosen[(1, self.snmp_id)]/1000
        self.dimy = self.printer.manager.prtInputMediaDimFeedDirChosen[(1, self.snmp_id)]/1000
        self.save()

    def __unicode__(self):
        return self.name


class Supply(models.Model):
    snmp_id = models.PositiveSmallIntegerField(editable=False)
    name = models.CharField(max_length=255, editable=False)
    printer = models.ForeignKey('Printer', editable=False, related_name='supplies')
    percent = models.IntegerField(editable=False)


    def update_from_snmp(self):
        self.name = self.printer.manager.prtMarkerSuppliesDescription[(1, self.snmp_id)]
        self.percent = self.printer.manager.prtMarkerSuppliesLevel[(1, self.snmp_id)]
        self.save()

    def __unicode__(self):
        return self.name


class Alert(models.Model):
    snmp_id = models.PositiveIntegerField(editable=False)
    description = models.TextField(editable=False)
    printer = models.ForeignKey('Printer', editable=False, related_name='alerts')


    def update_from_snmp(self):
        self.description = self.printer.manager.prtAlertDescription[(1, self.snmp_id)].decode("utf-8", "replace")
        self.save()

    def __unicode__(self):
        return self.description
