# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('snmp_id', models.PositiveIntegerField(editable=False)),
                ('description', models.TextField(editable=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PaperTray',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('snmp_id', models.PositiveSmallIntegerField(editable=False)),
                ('name', models.CharField(max_length=30, editable=False)),
                ('count', models.IntegerField(editable=False)),
                ('max_capacity', models.IntegerField(editable=False)),
                ('dimx', models.IntegerField(editable=False)),
                ('dimy', models.IntegerField(editable=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Printer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, editable=False)),
                ('domain_name', models.CharField(unique=True, max_length=255)),
                ('status', models.PositiveSmallIntegerField(editable=False, choices=[(b'???', 1), (b'running', 2), (b'warning', 3)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Supply',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('snmp_id', models.PositiveSmallIntegerField(editable=False)),
                ('name', models.CharField(max_length=255, editable=False)),
                ('percent', models.IntegerField(editable=False)),
                ('printer', models.ForeignKey(related_name='supplies', editable=False, to='printer.Printer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='papertray',
            name='printer',
            field=models.ForeignKey(related_name='paper_trays', editable=False, to='printer.Printer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alert',
            name='printer',
            field=models.ForeignKey(related_name='alerts', editable=False, to='printer.Printer'),
            preserve_default=True,
        ),
    ]
