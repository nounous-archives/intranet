# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('printer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='printer',
            name='status',
            field=models.PositiveSmallIntegerField(editable=False, choices=[(1, b'1???'), (2, b'running'), (3, b'warning'), (4, b'4???'), (5, b'down')]),
            preserve_default=True,
        ),
    ]
