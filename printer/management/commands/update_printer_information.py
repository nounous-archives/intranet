#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from printer.models import Printer

class Command(BaseCommand):
    help = u"Update printer information with SNMP protocol"

    def handle(self, *args, **options):
        for p in Printer.objects.all():
            p.update_printer_information()
            self.stdout.write(u"%s information updated" % p)

