#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template

register = template.Library()

CSS_CLASSES = ["overflow", "alert", "warning", "ok", "good"]

def _progress_class_generic(classes, value, arg=100):
    """Determines appropriate css class given a progress bar value and a maximum"""
    try:
        arg = float(arg)
        value = float(value)
    except ValueError:
        return "overflow"

    res = value/arg
    if (res < 0) or (res > 1):
        return classes[0]
    elif res <= 0.25:
        return classes[1]
    elif res <= 0.5:
        return classes[2]
    elif res <= 0.75:
        return classes[3]
    else:
        return classes[4]

def progress_class(value, arg=100):
    return _progress_class_generic(CSS_CLASSES, value, arg)

def progress_class_reverse(value, arg=100):
    return _progress_class_generic(CSS_CLASSES[:1] + CSS_CLASSES[:0:-1], value, arg)



register.filter('progress_class', progress_class)
register.filter('progress_class_reverse', progress_class_reverse)
