#!/bin/bash

# Évite d'enmurer son voisin …
umask 002

# Met à jour le dépot
git pull

# Vire les pyc résiduels
find -name "*.pyc" -delete

# Met à jour les fichiers statiques (utiles si serveur web de prod)
./manage.py collectstatic --noinput

# Migrate ?
./manage.py migrate

# Compile les fichiers de traduction
find -name locale -type d -execdir django-admin compilemessages \;
