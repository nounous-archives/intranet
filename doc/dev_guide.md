# Guide du développeur de l'intranet

## Créer une nouvelle application
Pour créer une nouvelle application vous aurez besoin d'effectuer
les étapes suivantes:
 1. Exécuter la commande `django-admin startapp nom_de_l'application` à la racine du dépôt
 2. Modifier le fichier `settings.py` pour rajouter une entrée à `INTRANET_APPS`:
    ```
    {
    'name': "nom_du_dossier_de_l'appli",
    'title':"Nom long de l'appli",
    'category':"Catégorie de l'appli",
    'label': u"Nom court de l'appli (page d'accueil et barre de navigation)",
    'help':"Lien vers une page descriptive de l'appli (optionnel)",
    'test': fonction_de_test_des_droits ,
    },
    ```

## Écrire des templates
Consulter le [guide d'écriture des templates](templates.md)

## Coding Style
Le mieux est de se référer au [guide officiel de Django](https://docs.djangoproject.com/en/1.7/internals/contributing/writing-code/coding-style/).

Petit rappel des conventions utilisé au Crans:
 * Méthodes ou variables: `nom_de_methode`
 * Classes: `NomDeLaClasse`

### Listes ###
 * Liste d'objets `foo`: `foos`
 * Élement de la liste précédente: `foo`
 * Syntaxe canonique pour les listes:
    ```
    for foo in foos:
      do_something
    ```

### Absence d'éléments ###
L'absence d'éléments constitue une des sources d'erreurs la plus courante en python.
Il faut donc adopter la convention suivante en l'absence d'élément:
 * Pour les types scalaires: `None`
 * Pour les types conteneurs: Le conteneur vide
   * Pour les listes: `[]`
   * Pour les dictionnaires: `{}`

Une telle convention permet à:
```
for element in elements:
  do_something
```
de ne rien faire en cas de liste vide, plutôt que de lever l'exception suivante:
```
TypeError: 'NoneType' object is not iterable
```
