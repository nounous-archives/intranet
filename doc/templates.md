Guide d'écriture des templates
==============================

Il est très probable que lors du développement de votre application vous ayez à écrire des templates.
Ce guide a pour but de faciliter la compréhension du système de templating au Crans.
Avant de vous lancer dans l'écriture d'un template
je vous recommande chaudement de lire rapidement la documentation sur le système de template django,
ainsi qu'un peu de documentation sur html et css.


Le template principal: template.html
------------------------------------

Au Crans le template principal a pour nom: `template.html`.
Le template le plus simple doit donc étendre `template.html`.
La syntaxe pour ce faire est très simple:
```
{% extends "template.html" %}
```

Ce fichier de base comporte 3 blocks principaux:
 * title
 * h1
 * content

Le block title contient le titre de la page, c'est-à-dire le titre affiché sur votre onglet de navigateur.

Le block h1 contient le titre qui sera affiché au dessus du rectangle central de l'intranet.

Le block content contient le contenu de votre template.

Le fichier de template le plus basique doit avoir cette tête:

```
{% extends "template.html" %}
{% block title %} Titre de ma page {% endblock content %}
{% block h1 %} Titre affiché au dessus du rectangle principal {% endblock h1 %}

{% block content %}
Mon beau code django-html
{% endblock content%}
```

Il existe aussi un quatrième block appelé `head` qui sert à ajouter du contenu personnalisé au `<head>` du html.
Si vous ne savez pas ce que c'est, ne le rajoutez pas !

Notez que le template principal du crans contient déjà toute les balises "d'initialisation du html" à savoir `<doctype>`, `<html>`, `<head>` `<body>`.
Il ne faut donc en aucun cas mettre ces balises à l'intérieur de votre template !


Le css de l'intranet
--------------------

Si vous essayez un template sur le serveur de dev vous remarquerez facilement qu'une bonne partie de la mise en forme est déjà faite.
En effet le css de l'intranet est très global, et dans 95% des cas vous n'auriez aucun fichier css à écrire.

Ces parties ont pour but d'explique la magie derrière le css global.

### Affichage par colonne ###
Le css est basé sur un framework nommé Skeleton ([http://getskeleton.com/]), qui est un framework `responsive`,
c'est-à-dire que l'affichage change fortement en fonction de la résolution d'écran.
Pour faire simple Skeleton utilise un système de 12 colonnes qui séparant les containeurs en partie égales.
Dès que l'on passe en dessous d'une certaine résolution on arrive sur la partie mobile.
Les colonnes disparaissent alors et s'empilent l'une au dessus des autres pour plus de lisibilité.

Pour utiliser le système de colonnes il suffit de mettre le contenu dans un `<div>` de classe `row`:
```html
<div class="row">
Mon code html
</div>
```
Ensuite il faut déclarer les colonnes et leur largeur à l'aide de `<div>` de classe `"nombre_en_anglais" columns`:
```html
<div class="row">
  <div class="six columns">
  Code html de la première colonne
  </div>
  <div class="two columns">
  Code html de la deuxième colonne
  </div>
  <div class="four columns">
  Code html de la troisième colonne
  </div>
</div>
```
Et c'est tout, ça suffit à utiliser le système de colonne.
Un exemple plus poussé est donné dans la partie: Cas particulier des formulaires.

### La balise `<h2>` ###
Si vous voulez séparer votre page en section il suffit de mettre le nom de vos sections dans des balises `<h2>` à la racine de votre code.
Le css de base mettra automatiquement des lignes de séparation.
Exemple:
```html
{% block content %}
<h2>Titre de ma première section</h2>
  Code
  ...
<h2>Titre de ma deuxième section</h2>
  Code
  ...
{% endblock content %}
```

### Les tableaux ###
Par défaut les tableaux prennent toute la largeur et sont zébrés.
Quand l'utilisateur passe sa souris sur une ligne celle-ci se colore légèrement.

ATTENTION: Ne jamais utiliser des tableaux pour faire de la mise en page. Ça ne sert pas à ça !
Utilisez le système de colonnes à la place.

Voici un exemple de tableau bien déclaré :
```html
<table>
<thead>
  <tr>
    <th>Header 1</th>
    <th>Header 2</th>
    <th>Header 3</th>
  </tr>
</thead>
<tbody>
{% for itérable in truc_à_itérer %}
  <tr>
    <td>{{ itérable.truc }}</td>
    <td>{{ itérable.muche }}</td>
    <td>{{ itérable.plop }}</td>
  </tr>
{% endfor %}
</tbody>
</table>
```

#### Version mobile ####
Sur mobile les tableaux passent très mal car ces appareils disposent de peu de résolution en largeur.
Du coup il existe une astuce pour rendre son tableau mobile friendly.
Il faut d'abord rajouter un attribut mobile-header pour tout les `<td>` du `<tbody>`.
Puis faire hériter notre tableaux de la classe `mobile-friendly`.

L'exemple précédent se transforme en :
```html
<table class="mobile-friendly">
<thead>
  <tr>
    <th>Header long 1</th>
    <th>Header long 2</th>
    <th>Header long 3</th>
  </tr>
</thead>
<tbody>
{% for itérable in truc_à_itérer %}
  <tr>
    <td mobile-header="Header court 1">{{ itérable.truc }}</td>
    <td mobile-header="Header court 2">{{ itérable.muche }}</td>
    <td mobile-header="Header court 3">{{ itérable.plop }}</td>
  </tr>
{% endfor %}
</tbody>
</table>
```

Ces modifications permettent au template de créer des headers à gauche des champs, qui seront alors ordonnés en colonnes.

### Les boutons ###
Il existe 4 classe de bouton déjà stylisés pour les `<bouton>` ou les `<input type="submit">`
 * Celle par défaut, ou de classe `bouton`. Elle n'a pas d'usage en particulier.
 * La classe `button-add` qui indique que l'appuie sur le bouton va entraîner une création.
 * La classe `button-del` qui indique que l'appuie sur le bouton va entraîner une suppression.
 * La classe `button-cancel` qui indique que l'appuie sur le bouton va annuler les changements.

### Barres de progression ###
Le CSS intégre des barres de progressions qui permettent de mettre en valeur
les données de taux d'utilisation.

#### Classes ####
Pour utiliser ces barres il suffit de créer un div containeur de classe
`progress-container` ainsi qu'un div pour la barre à afficher de classe
`progress-bar` et d'une classe de statut parmis:
 * `overflow`
 * `alert`
 * `warning`
 * `ok`
 * `good`


```html
<div class="progress-container">
  <div class="progress-bar good">
    Bravo tu n'as pas dépassé ton quota !
  </div>
</div>
```

Note: Comme souvent on ne connais pas la largeur à l'avance
on peut s'autoriser **exceptionnellement** à utiliser l'attribut
`style` dans le code html:
```html
<div class="progress-bar good" style="width: {{ ma_largeur }}%;">
  Tu utilises {{ mon_quota }}% de ton quota
<div>
```

#### Raccourcis ####
En pratique il est utile de pouvoir adapter la classe de statut de la
barre de progression en fonction du pourcentage afficher.
Afin d'éviter la duplication de code il existe des filtres dans l'intranet
pour pouvoir choisir la classe de statut automatiquement.

Il faut d'abord pour cela charger les filtres supplémentaire de l'intranet
à l'aide de la balise: `{% load custom_filters %}`.
Puis l'on peut utiliser un des deux filtres disponibles:
 * `progress_class`:`max`, qui vaut `good` à `max` et `alert` à 0.
 * `progress_class_reverse`:`max`, qui vaut `alert` à `max` et `good` à 0.
Où `max` est le maximum que notre valeur filtrée peut atteindre.
En cas de dépassement nos filtres vaudront `overflow`.
Si `max` n'est pas précisé la valeur 100 sera utilisé.


Exemple d'utilisation des barres de progression (extrait d'impression)
```html
{% load custom_filters %}
<div class="progress-container">
  <div class="progress-bar {{ tray.count|progress_class:tray.max_capacity }}"
       style="width:{% widthratio tray.count tray.max_capacity 100 %}%;">
    {{ tray.count }}/{{ tray.max_capacity }}
  </div>
</div>
```
Dans les cas où la valeur calculée n'est pas un pourcentage, on peut faire une
mise à l'échelle à l'aide de la balise Django [widthratio](https://docs.djangoproject.com/fr/1.7/ref/templates/builtins/#widthratio)


Cas particulier des formulaires
-------------------------------
Comme les formulaires c'est un peu complique le mieux est d'aller voir `form_minimal.html.example` et `form_complete.html.example` pour des exemples de formulaire.

Le premier est pour les flemmard et ne comprend que l'essentiel.
Il est possible que le rendu ne soit pas terrible sur la version mobile.

Le deuxième comprend le système de colonnes et la séparation en section.
Il a un bon rendu sur la version mobile.

Ces exemples sont disponibles dans le dossier `templates`
