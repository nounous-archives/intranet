# -*- coding: utf-8 -*-
# Récriture de l'app compte
# Enrichissement de celle-ci.
# Gabriel Détraz detraz@crans.org
# Charlie Jacomme jacomme@crans.org

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
import subprocess
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponseRedirect
from django.template import RequestContext
from intranet import conn_pool
from django.utils import timezone
from django.core.urlresolvers import reverse

from django.contrib import messages
from django.views.generic import View

import datetime
from dateutil.relativedelta import relativedelta

from passlib.apps import ldap_context
import lc_ldap.crans_utils
from lc_ldap import shortcuts
from intranet import settings

from gestion.config import factures, cotisation

from forms import BaseCompteForm, CompteForm, BasePassForm, PassForm, MailForm, AliasForm, SoldeForm, VenteForm, AdhesionForm, CguForm, ComptecransForm, DemenagementForm, NewCompteForm, EmenagementForm, NewClubForm
from django.forms.utils import ErrorList

from intranet.mixins import CableurOrSelfMixin, CableurMixin

class RedirectHomeMixin(object):
    """
      Mixin permettant de rediriger vers le compte de l'utilisateur modifié et non du request.user
    """
    def dispatch(self, request, uid, ptype=u'adh', next=None, *args, **kwargs):
        if not next:
            if not uid:
                next = reverse('compte:afficher')
            if ptype == 'club':
                next = reverse('compte:afficher_club', args=(uid,))
            else:
                next = reverse('compte:afficher', args=(uid,))
        return super(RedirectHomeMixin, self).dispatch(request, uid=uid, next=next, ptype=ptype, *args, **kwargs)

def get_luser(request, uid, ptype, mode='ro'):
    if ptype == 'adh':
        return conn_pool.get_conn(request.user).search(u'aid=%s' % uid, mode=mode)[0]
    elif ptype == 'club':
        return conn_pool.get_conn(request.user).search(u'cid=%s' % uid, mode=mode)[0]

def get_admin_luser(request, uid, ptype, mode='ro'):
    ldap = shortcuts.lc_ldap_admin()
    if ptype == 'adh':
        return ldap.search(u'aid=%s' % uid, mode=mode)[0]
    elif ptype == 'club':
        return ldap.search(u'cid=%s' % uid, mode=mode)[0]

class ClubView(CableurOrSelfMixin, View):
    """
      Classe de base pour l'affichage d'un club. Doit être appellé avec un cid.
    """
    template_name = 'compte/affichage_club.html'

    def get(self, request, cid, *args, **kwargs):
        # On a juste a peupler le form avec les informations de l'utilisateur
        luser = conn_pool.get_conn(request.user).search(u'cid=%s' % cid)[0]
        # Si il n'y a pas de compte crans derrière, uid vaut false et l'app de cablage n'affiche pas tout
        uid = luser.get('uid', False)
        form = BaseCompteForm(luser)
        context = {
            'form': form,
            'now': timezone.now(),
            'luser': luser,
            'comptecrans': uid,
            }
        return render(request, self.template_name, context)

    def post(self, request, cid, *args, **kwargs):
        luser = conn_pool.get_conn(request.user).search(u'cid=%s' % cid)[0]
        # Si il n'y a pas de compte crans derrière, uid vaut false et l'app de cablage n'affiche pas tout
        uid = luser.get('uid', False)
        form = BaseCompteForm(luser, request.POST)
        if form.is_valid():
            luser = conn_pool.get_conn(request.user).search(u'cid=%s' % cid, mode="w")[0]

            # On boucle sur les champs modifiés pour mettre à jour ldap
            for field in form.changed_data:
                try:
                    luser[field] = form.cleaned_data[field]
                except ValueError as e:
                    elist = form._errors.setdefault(field, ErrorList())
                    elist.append(e)

            # Il faut créer l'historique avant de sauvegarder.
            luser.history_gen()
            luser.save()
            messages.success(request, _(u"""Les modifications ont été prises en compte"""))

        context = {
            'form': form,
            'now': timezone.now(),
            'luser': luser,
            'comptecrans': uid,
            }
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def dispatch(self, request, cid=None, *args, **kwargs):
        if not cid:
            luser = conn_pool.get_user(request.user)
            cid = luser['cid'][0]
        return super(ClubView, self).dispatch(request, cid, *args, **kwargs)

afficher_club = ClubView.as_view()


class CompteView(CableurOrSelfMixin, View):
    """
      Affichage d'un adhérent. Necessite un aid.
    """
    template_name = 'compte/affichage.html'

    def get(self, request, aid, *args, **kwargs):
        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % aid)[0]
        # Si il n'y a pas de compte crans derrière, uid vaut false et l'app de cablage n'affiche pas tout
        uid = luser.get('uid', False)
        if uid:
            redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--read", "--name=%s" % luser['uid'][0]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            mailredirect = redirection_mail.stdout.readlines()
            if mailredirect:
                mailredirect = mailredirect[0]
            else:
                mailredirect = "N/A"
        else:
            mailredirect = "N/A"

        form = CompteForm(luser)
        context = {
            'form': form,
            'now': timezone.now(),
            'luser': luser,
            'mailredirect': mailredirect,
            'comptecrans': uid,
            }
        return render(request, self.template_name, context)

    def post(self, request, aid, *args, **kwargs):
        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % aid)[0]
        uid = luser.get('uid', False)
        form = CompteForm(luser, request.POST)
        if form.is_valid():
            luser = conn_pool.get_conn(request.user).search(u'aid=%s' % aid, mode="w")[0]
            if form.apply(luser):
                # Il faut créer l'historique avant de sauvegarder.
                luser.history_gen()
                luser.save()
                messages.success(request, _(u"""Les modifications ont été prises en compte"""))

        context = {
            'form': form,
            'now': timezone.now(),
            'luser': luser,
            'comptecrans': uid,
            }
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def dispatch(self, request, aid=None, *args, **kwargs):
        if not aid:     # Si aid=None, on renvoit celui de l'user
            luser = conn_pool.get_user(request.user)
            aid = luser['aid'][0]
        return super(CompteView, self).dispatch(request, aid, *args, **kwargs)

afficher = CompteView.as_view()

class ChgPassView(CableurOrSelfMixin, RedirectHomeMixin, View):
    """
      Classe de base pour le changement d'un password
    """
    template_name = "compte/chgpass.html"

    def get(self, request, uid, ptype, cablage, *args, **kwargs):
        # Selon si on est cableur on a besoin ou pas de rentrer l'ancien mdp
        if cablage:
            form = BasePassForm()
        else:
            form = PassForm()
        return render(
            request,
            self.template_name,
            {
                'form': form,
                'ptype': ptype,
                'ide': uid,
                'cablage': cablage,
            },
        )

    @method_decorator(sensitive_post_parameters())
    def post(self, request, uid, ptype, cablage, next='/compte/', *args, **kwargs):
        # Selon si on est cableur on a besoin ou pas de rentrer l'ancien mdp
        if cablage:
            form = BasePassForm(request.POST)
        else:
            form = PassForm(request.POST)
        luser = get_luser(request, uid, ptype, 'w')
        if form.is_valid():
            # Soit on est en train de cabler soit l'ancien mot de passe est bon
            if cablage or ldap_context.verify(
                    unicode(form.cleaned_data['passwdexists']),
                    unicode(luser['userPassword'][0])
            ):
                if form.apply(luser):
                    luser.history_gen()
                    luser.save()
                    messages.success(request, _(u"""Le mot de passe a bien été changé"""))
                    return redirect(next)
            else: # l'ancien mot de passe est éroné
                errors = form._errors.setdefault("passwdexists", ErrorList())
                errors.append(_(u"Ancien mot de passe erroné."))

        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid, 'cablage': cablage})

    @method_decorator(login_required)
    def dispatch(self, request, uid, ptype, *args, **kwargs):
        luser = conn_pool.get_user(request.user)
        cablage = False
        # Si je ne me modifie pas moi même et que je suis cableur => Cablage
        if ptype == 'adh' and luser.get("aid", [""])[0] != uid and request.user.has_perm('auth.crans_cableur'):
            cablage = True
        if ptype == 'club' and request.user.has_perm('auth.crans_cableur'):
            cablage = True
        return super(ChgPassView, self).dispatch(request, uid=uid, ptype=ptype, cablage=cablage, *args, **kwargs)

chgpass = ChgPassView.as_view()


class RedirectionView(CableurOrSelfMixin, RedirectHomeMixin, View):
    """
        Vue pour la gestion de la redirection mail
    """
    template_name = 'compte/redirection.html'

    def get(self, request, uid, *args, **kwargs):
        # On a juste a peupler le form avec les informations de l'utilisateur
        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % uid)[0]
        form = MailForm(luser)
        return render(request, self.template_name, {'form': form, 'luser': luser})

    def post(self, request, uid, generate=False, next='/compte/', *args, **kwargs):
        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % uid)[0]
        form = MailForm(luser, request.POST)
        if form.is_valid():
        # La redirection n'est pas un champ ldap
            if 'mailredirect' in form.changed_data:
                mailredirect = unicode(form.cleaned_data['mailredirect'])
                if mailredirect != "":
                    if not generate:
                        redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--write", "--mail=%s" % mailredirect, "--name=%s" % luser['uid'][0]])
                    else:
                        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % uid, mode='w')[0]
                        with luser as adh:
                            adh['mailExt'] = mailredirect
                            adh.history_gen()
                            adh.save()
                    messages.success(request, _(u"""Votre redirection a bien été prise en compte"""))
                return redirect(next)

        return render(request, self.template_name, {'form': form, 'luser': luser})

redirection = RedirectionView.as_view()

class DeleteForwardView(CableurOrSelfMixin, RedirectHomeMixin, View):
    """
        Classe pour la suppresion d'une redirection mail.
    """
    def get(self, request, uid, next="/compte/", *args, **kwargs):
        luser = conn_pool.get_conn(request.user).search(u'aid=%s' % uid, mode='w')[0]
        try:
            redirection_mail = subprocess.Popen(
                [
                    "sudo",
                    "-n",
                    "/usr/scripts/utils/forward.py",
                    "--delete",
                    "--name=%s" % luser['uid'][0]
                ]
            )
            messages.success(
                request,
                _(u"Votre redirection a été supprimée, pensez à consulter régulièrement votre adresse Cr@ns !")
            )
        except ValueError as e:
            messages.error(request, e)
        return redirect(next)

deleteforward = DeleteForwardView.as_view()

class DeleteAliasView(CableurMixin, RedirectHomeMixin, View):
    """
        Classe pour la réinitialisation du mot de passe et l'impression d'un ticket
    """
    def get(self, request, uid, ptype, alias_id, next='/compte/', *args, **kwargs):
        # Vue de suppresion d'un alias. N'a pas de visuel, redirige juste vers le home
        luser = get_luser(request, uid, ptype, 'w')
        try:
            luser['mailAlias'].remove(luser['mailAlias'][int(alias_id)])
            luser.history_gen()
            luser.save()
            messages.success(request, _(u"""L'alias a été retiré avec succès"""))
        except ValueError as e:
            # Le message d'une ValueError est directement dans e
            messages.error(request, e)
        return redirect(next)

delete_alias = DeleteAliasView.as_view()

class AliasView(CableurOrSelfMixin, RedirectHomeMixin, View):
    """
      Classe de base pour les alias
    """
    template_name = "compte/alias.html"

    def get(self, request, uid, ptype, *args, **kwargs):
        luser = get_luser(request, uid, ptype)
        form = AliasForm()
        return render(request, self.template_name, {'form': form, 'luser': luser, 'ptype': ptype, 'ide': uid})

    def post(self, request, uid, ptype, next='/compte/', *args, **kwargs):
        form = AliasForm(request.POST)
        luser = get_luser(request, uid, ptype, 'w')
        if form.is_valid():
            # Pour les Alias mail, on ne permet que l'ajout, pas la suppression
            if 'mailAlias' in form.changed_data:
                try:
                    luser['mailAlias'].append(form.cleaned_data['mailAlias'])
                    luser.history_gen()
                    luser.save()
                    messages.success(request, _(u"""L'alias a été ajouté avec succès"""))
                    return redirect(next)
                except ValueError as e:
                     # Le message d'une ValueError est directement dans e
                    elist = form._errors.setdefault('mailAlias', ErrorList())
                    elist.append(e)
        return render(request, self.template_name, {'form': form, 'luser': luser, 'ptype': ptype, 'ide': uid})

alias = AliasView.as_view()



class SoldeView(CableurMixin, RedirectHomeMixin, View):
    """
      Classe de base pour les modifications de solde
    """
    template_name = "compte/solde.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        form = SoldeForm()
        return render(request,
            self.template_name,
            {
                'form': form,
                'ptype': ptype,
                'ide': uid,
            }
        )

    def post(self, request, uid, ptype, next='/compte/', *args, **kwargs):
        form = SoldeForm(request.POST)
        luser = get_luser(request, uid, ptype, 'w')
        if form.is_valid():
            fact = {
                'modePaiement': [form.cleaned_data['mode']],
                'article': [u'%s~~%s~~1~~%s' % ('SOLDE', 'Rechargement de solde ' + form.cleaned_data['commentaire'],float(form.cleaned_data['montant']))],
            }
            with conn_pool.get_conn(request.user).newFacture(luser.dn, fact) as facture:
                try:
                    facture.crediter()
                except ValueError as e:
                # Cas où le solde n'est pas suffisant
                    messages.error(request, e)
                    return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid})
                fid = facture['fid'][0]
            messages.success(request, _(u"""Vente effectuée, fid=%s (à conserver)""") % fid )
            return redirect(next)

        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid})

solde = SoldeView.as_view()

class VenteView(CableurMixin, RedirectHomeMixin, View):
    """
      Classe de base pour la vente d'objets
    """
    template_name = "compte/vente.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        form = VenteForm()
        articles = factures.ITEMS
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid, 'articles': articles})

    def post(self, request, uid, ptype, next='/compte/', *args, **kwargs):
        form = VenteForm(request.POST)
        luser = get_luser(request, uid, ptype, 'w')
        articles = factures.ITEMS

        if form.is_valid():
            fid = []
            # Boucle sur les différents articles
            for art in form.cleaned_data['articles']:
		details = factures.ITEMS[art]
                fact = {
                'modePaiement': [form.cleaned_data['mode']],
                'article': [u'%s~~%s~~1~~%s' % (art, details['designation'] + " " + form.cleaned_data['commentaire'],details['pu'])],
                }
                with conn_pool.get_conn(request.user).newFacture(luser.dn, fact) as facture:
                    try:
                        facture.crediter()
                    except ValueError as e:
                    # Cas où le solde n'est pas suffisant
                        messages.error(request, e)
                        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid, 'articles': articles})
                    fid.append(unicode(facture['fid'][0]))
            messages.success(request, _(u"""Vente effectuée, fid=%s (à conserver)""") % (', '.join(fid)) )
            return redirect(next)
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid, 'articles': articles})



vente = VenteView.as_view()



class AdhesionView(CableurOrSelfMixin, RedirectHomeMixin, View):
    """
        Classe de base pour l'adhésion
    """
    template_name = "compte/adhesion.html"
    def get(self, request, uid, ptype, cablage, *args, **kwargs):
        form = AdhesionForm(request.POST, is_cableur=cablage)
        cgu = CguForm(request.POST)
        return render(
            request,
            self.template_name,
            {
                'form': form,
                'cgu': cgu,
                'ptype': ptype,
                'ide': uid,
                'cablage': cablage,
            }
        )

    def post(self, request, uid, ptype, cablage, next='/compte/', *args, **kwargs):
        """
            Si on est dans une requête POST
        """
        def connexion_possible(nbrmois, luser):
            # Determine le nombre de mois possible avant la readhesion
            try:
                reste_adhesion = relativedelta(luser.fin_adhesion().value, luser.fin_connexion().value)
                if reste_adhesion.months < 0:
                    nbrmois = 0
                    return nbrmois
                if nbrmois > reste_adhesion.months and reste_adhesion.years == 0:
                    if reste_adhesion.days > 15:
                        nbrmois = reste_adhesion.months + 1
                    else:
                        nbrmois = reste_adhesion.months
            except AttributeError:
                # La fin d'adhésion ne peut être évaluée, pas de connexion possible
                nbrmois = 0
            return nbrmois

        def prix(nbrmois, luser, ptype, cablage):
            # Détermine en fonction du nombre de mois si l'adhésion va étre faite et calcul le prix
            readhesion = False
            now = lc_ldap.crans_utils.localized_datetime()
            prix = min(nbrmois * cotisation.contribution, cotisation.plafond_contribution)
            try:
                delta = luser.fin_adhesion().value - now
                delta = delta.days
            # Si il n'y a pas de fin adh, on renvoie 0
            except AttributeError:
                delta = 0
            if delta < cotisation.delai_readh_jour:
                prix += cotisation.cotisation
                readhesion = True
            # Pour les clubs, c'est gratuit
            if ptype == 'club':
                prix = 0

            return nbrmois, prix, readhesion

        # On traite le post
        form = AdhesionForm(request.POST, is_cableur=cablage)
        cgu = CguForm(request.POST)
        luser = get_admin_luser(request, uid, ptype, 'w')
        if form.is_valid():
            #EN 2 etapes, on renvoie
            if 'confirm' in request.POST.keys():
                ######## Facturation de la connexion ########
                mois = form.cleaned_data['nbrmois']
                mois, prix, readhesion = prix(mois, luser, ptype, cablage)
                fact_cotis = cotisation.dico_cotis(mois)
                # On evite de créer des factures de 0 mois de connexions, pour les clubs, on ne fait pas de connexion
                if mois > 0 and ptype != 'club':
                    fact = {
                    'modePaiement': [form.cleaned_data['mode']],
                    'article': [u'%s~~%s~~%s~~%s' % (fact_cotis['code'], fact_cotis['designation'], fact_cotis['nombre'], fact_cotis['pu'])],
                    }
                else:
                    fact = {
                    'modePaiement': [form.cleaned_data['mode']],
                    'article': [],
                    }
                ########## Réadhésion ###########
                # Si la fonct d readhésion a repondu True, on réadhère aussi
                if readhesion and cgu.is_valid():
                    if ptype == 'club':
                        dico_adh = cotisation.dico_adh_club
                    else:
                        dico_adh = cotisation.dico_adh
                    fact['article'].append(u'%s~~%s~~%s~~%s' % (dico_adh['code'], dico_adh['designation'], dico_adh['nombre'], dico_adh['pu']))
                elif readhesion and not cgu.is_valid():
                    # Les CGU sont obligatoires
                    messages.error(request, _(u"La case d'acceptation des textes est obligatoire"))
                    return render(
                            request,
                            self.template_name,
                            {'form': form,
                             'cgu': cgu,
                             'ptype': ptype,
                             'ide': uid,
                             'cablage': cablage},
                            )
                elif mois == 0 or ptype == 'club':
                    # Dans ce cas il n'y a rien à faire
                    messages.error(request, _(u"La réadhésion n'est possible que moins d'un mois avant l'expiration, merci de repasser"))
                    return redirect(next)
                ####### Ecriture de la facture ############
                with shortcuts.lc_ldap_admin().newFacture(luser.dn, fact) as facture:
                    try:
                        facture.adhesion_connexion()
                    except ValueError as e:
                        # Cas où le solde n'est pas suffisant
                        messages.error(request, e)
                        return redirect(next)
                    fid = unicode(facture['fid'][0])
                    if readhesion:
                        fin_adh = unicode(facture['finAdhesion'][0])
                        debut_adh = unicode(facture['debutAdhesion'][0])
                    if mois > 0 and ptype != 'club':
                        fin_conn = unicode(facture['finConnexion'][0])
                        debut_conn = unicode(facture['debutConnexion'][0])
                #Hack : on recharge l'objet ldap
                luser = get_admin_luser(request, uid, ptype, 'w')
                ######## Mise à jour des attributs adhérent #########
                # On met à jour les valeurs fin_adh et fin_conn de l'adhérent
                with luser as adh:
                    if mois > 0 and ptype != 'club':
                        adh['finConnexion'].append(fin_conn)
                        adh['debutConnexion'].append(debut_conn)
                    if readhesion:
                        adh['finAdhesion'].append(fin_adh)
                        adh['debutAdhesion'].append(debut_adh)
                    adh.history_gen()
                    adh.save()
                messages.success(
                        request,
                        _(u"""Vente effectuée, fid=%s (à conserver)""") % (fid)
                    )
                return redirect(next)
            else:
                mois = form.cleaned_data['nbrmois']
                mois, prix, readhesion = prix(mois, luser, ptype, cablage)
                if not readhesion and mois == 0:
                    messages.error(
                            request,
                            _(u"La réadhésion est impossible plus d'un mois avant la fin de l'adhésion. Vous pouvez soit prolonger votre connexion jusqu'à la fin de votre adhésion, soit revenir un mois avant la fin votre adhésion.")
                        )
                    return render(
                            request,
                            self.template_name,
                            {'form': form,
                             'cgu': cgu,
                             'ptype': ptype,
                             'ide': uid},
                        )
                return render(
                        request,
                        self.template_name,
                        {'form': form,
                         'cgu': cgu,
                         'ptype': ptype,
                         'ide': uid,
                         'confirm': True,
                         'prix': prix,
                         'readhesion': readhesion,
                         'nbrmois': mois,
                         'cablage': cablage},
                        )
        return render(
                request,
                self.template_name,
                {'form': form,
                 'cgu': cgu,
                 'ptype': ptype,
                 'ide': uid,
                 'cablage': cablage},
                )


    @method_decorator(login_required)
    def dispatch(self, request, uid, ptype, *args, **kwargs):
        luser = conn_pool.get_user(request.user)
        cablage = False
        if request.user.has_perm('auth.crans_cableur'):
            cablage = True
        return super(AdhesionView, self).dispatch(request, uid=uid, ptype=ptype, cablage=cablage, *args, **kwargs)


adhesion = AdhesionView.as_view()


class ComptecransView(CableurMixin, View):
    """
      Classe de base pour la création de compte crans
    """
    template_name = "compte/comptecrans.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        luser = get_luser(request, uid, ptype)
        form = ComptecransForm(luser, conn_pool.get_conn(request.user), ptype)
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid})

    def post(self, request, uid, ptype, *args, **kwargs):
        luser = get_luser(request, uid, ptype, 'w')
        form = ComptecransForm(luser, conn_pool.get_conn(request.user), ptype, request.POST)
        if form.is_valid():
            if form.apply(luser): #Si on arrive à appliquer les modifs à l'objet
                luser.history_gen()
                luser.save()
                messages.success(request, _(u"""Compte Crans crée, choisir un mot de passe"""))
                if ptype == 'adh':
                    redir_url = reverse('compte:redirection', args=(luser['aid'][0], True, '/compte/'))
                    return redirect(reverse('compte:chgpass', args=(u'adh', luser['aid'][0], redir_url)))
                else:
                    return redirect(reverse('compte:chgpass', args=(ptype,uid)))
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid})

comptecrans = ComptecransView.as_view()


class DemenagementView(CableurMixin, RedirectHomeMixin, View):
    """
      Classe de base pour le déménagement
    """
    template_name = "compte/demenagement.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        luser = get_luser(request, uid, ptype)
        form = DemenagementForm(luser)
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid})

    def post(self, request, uid, ptype, confirm = "", next='/compte/', *args, **kwargs):
        luser = get_luser(request, uid, ptype, 'w')
        form = DemenagementForm(luser, request.POST)
        if form.is_valid():
            lconn =  conn_pool.get_conn(request.user)
            res = form.apply(luser, confirm, lconn)
            if res == 'confirm':
                confirm = 'confirm'
            elif res:
                luser.save()
                messages.success(request, _(u"""Déménagement effectué"""))
                return redirect(next)
        return render(request, self.template_name, {'form': form, 'ptype': ptype, 'ide': uid, 'confirm' : confirm})

demenagement = DemenagementView.as_view()


class DeleteCompteView(CableurMixin, View):
    """
      Classe pour la suppression d'un compte
    """
    template_name = "compte/delete.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        return render(request, self.template_name, {'ptype': ptype, 'ide': uid})

    def post(self, request, uid, ptype, confirm = "", *args, **kwargs):
        luser = get_luser(request, uid, ptype, 'w')
        for m in luser.machines():
            m.delete(login = request.user.username)
        luser.delete(login = request.user.username)
        messages.success(request, _(u"""Compte supprimé"""))
        return redirect(reverse('cablage:afficher'))


delete = DeleteCompteView.as_view()

class CreateCompteView(CableurMixin, View):
    """
        Classe pour la création d'un adherent au crans. Au premier affichage, on ne voit pas encore le formulaire de comptecrans.
        La hidden value step2 est ajouté dans ce cas au template. Lors du post, si on a step2
        on affiche le formulaire de comptecrans avec un login prégénéré grace
        au informations du premier forms.
    """
    form1 = NewCompteForm
    form2 = EmenagementForm
    template_name = "compte/create.html"
    def get(self, request, ptype, *args, **kwargs):
        return render(request, self.template_name, {'form1' : self.form1(), 'form2' : self.form2(), 'ptype' : ptype})

    def post(self, request, ptype, confirm = "",  *args, **kwargs):
        form1 = self.form1(data=request.POST)
        form2 = self.form2(data=request.POST)
        form3 = None
        comptecrans = False
        if form1.is_valid() and form2.is_valid():
            if 'comptecrans' in request.POST.keys():
                comptecrans = True
            if ptype == u"club":
                new_adherent = conn_pool.get_conn(request.user).newClub
            else:
                new_adherent = conn_pool.get_conn(request.user).newAdherent
            with new_adherent({}) as adherent:
                lconn =  conn_pool.get_conn(request.user)
                if 'step2' in request.POST.keys() and form1.apply(adherent):
                    if not form1.check_date():
                        messages.error(request, _(u"L'adhérent est mineur, merci de faire signer une décharge  par le responsable légal"))
                    form3 = ComptecransForm(ldap_user = adherent, ldap_conn=lconn, ptype =  ptype)
                else:
                    form3 = ComptecransForm(data=request.POST)
                    if form3.is_valid():
                        # Si ce n'est pas un club, on vérifie la présence d'un mail de contact
                        if not ptype==u"club":
                            if not form3.cleaned_data['login'] and not form1.cleaned_data['mail']:
                                messages.error(request, _(u"Il faut soit un mail extérieur soit un compte crans"))
                                return render(request, self.template_name, {'form1' : form1, 'form2' : form2, 'form3' : form3, 'confirm' : confirm, 'ptype' : ptype, 'comptecrans': comptecrans})
                        if form1.apply(adherent) and form3.apply(adherent):
                            res = form2.apply(adherent, confirm, lconn)
                            if res == 'confirm':
                                confirm = 'confirm'
                            elif res:
                                adherent.create()
                                messages.success(request, _(u"""Compte créé"""))
                                if ptype == u"club":
                                    uid = adherent['cid'][0]
                                    redir_url = reverse('compte:adhesion', args=(ptype, uid))
                                elif comptecrans:
                                    uid = adherent['aid'][0]
                                    adh_url = reverse('compte:adhesion', args=(ptype, uid))
                                    redir_url = reverse('compte:redirection', args=(uid, True, adh_url))
                                else:
                                    uid = adherent['aid'][0]
                                    redir_url = reverse('compte:adhesion', args=(ptype, uid))
                                if comptecrans:
                                    return redirect(reverse('compte:chgpass', args=(ptype, uid, redir_url)))
                                else:
                                    return redirect(redir_url)

        return render(request, self.template_name, {'form1' : form1, 'form2' : form2, 'form3' : form3, 'confirm' : confirm, 'ptype' : ptype, 'comptecrans': comptecrans})

    @method_decorator(login_required)
    def dispatch(self, request, ptype=u"adh", *args, **kwargs):
        if ptype == u"club":
            self.form1 = NewClubForm
        return super(CreateCompteView, self).dispatch(request, ptype=ptype, *args, **kwargs)

create = CreateCompteView.as_view()


#TODO:
# * proposer d'envoyer un email de réinitialisation à l'adhérent
# * rattraper OSError sur l'intranet de test à la tentative d'impression
#   de ticket

class ResetPassView(CableurMixin, RedirectHomeMixin, View):
    """
        Classe pour la réinitialisation du mot de passe et
        l'impression d'un ticket
    """
    template_name = "compte/resetpass.html"
    def get(self, request, uid, ptype, *args, **kwargs):
        user = get_luser(request, uid, ptype)
        return render(
            request,
            self.template_name,
            { 'user' : user },
        )

    def post(self, request, uid, next='/compte/', *args, **kwargs):
        # Si la réponse reçu n'est pas 'y' aka 'Yes' ou 'Oui'
        if not 'y' in request.POST:
            return redirect(next)
        #Sinon on continue
        #et on imprime un ticket
        subprocess.call([
            '/usr/scripts/cransticket/dump_creds.py',
            '--pass',
            '--forced',
            'aid=%s' % uid,
        ])
        messages.success(
            request,
            _(u"""Mot de passe réinitialisé, ticket en cours d'impression.""")
        )
        return redirect(next)

resetpass = ResetPassView.as_view()
