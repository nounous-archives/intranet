from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.afficher, name='afficher'),
    url('^adh/$', views.afficher, name='afficher'),
    url('^adh/(?P<aid>[0-9]+)/$', views.afficher, name='afficher'),
    url('^club/(?P<cid>[0-9]+)/$', views.afficher_club, name='afficher_club'),
    url('^club/$', views.afficher_club, name='afficher_club'),
    url('^chgpass/$', views.chgpass, name='chgpass'),
    url('^chgpass/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.chgpass, name='chgpass'),
    url('^chgpass/(?P<ptype>club)/(?P<uid>[0-9]+)/(?P<next>.*)/$', views.chgpass, name='chgpass'),
    url('^chgpass/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.chgpass, name='chgpass'),
    url('^chgpass/(?P<ptype>adh)/(?P<uid>[0-9]+)/(?P<next>.*)/$', views.chgpass, name='chgpass'),
    url('^redirection/$', views.redirection, name='redirection'),
    url('^redirection/(?P<uid>[0-9]+)/$', views.redirection, name='redirection'),
    url('^redirection/(?P<uid>[0-9]+)/(?P<generate>True)/(?P<next>.*)/$', views.redirection, name='redirection'),
    url('^delete_forward/(?P<uid>[0-9]+)/$', views.deleteforward, name='delete_forward'),
    url('^alias/$', views.alias, name='alias'),
    url('^alias/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.alias, name='alias'),
    url('^alias/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.alias, name='alias'),
    url('^delete_alias/(?P<uid>[0-9]+)/(?P<ptype>adh)/(?P<alias_id>[0-9]+)/$',
views.delete_alias, name='delete_alias'),
    url('^delete_alias/(?P<uid>[0-9]+)/(?P<ptype>club)/(?P<alias_id>[0-9]+)/$',
views.delete_alias, name='delete_alias'),
    url('^solde/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.solde, name='solde'),
    url('^solde/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.solde, name='solde'),
    url('^vente/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.vente, name='vente'),
    url('^vente/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.vente, name='vente'),
    url('^adhesion/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.adhesion, name='adhesion'),
    url('^adhesion/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.adhesion, name='adhesion'),
    url('^comptecrans/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.comptecrans, name='comptecrans'),
    url('^comptecrans/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.comptecrans, name='comptecrans'),
    url('^demenagement/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.demenagement, name='demenagement'),
    url('^demenagement/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.demenagement, name='demenagement'),
    url('^demenagement/(?P<ptype>club)/(?P<uid>[0-9]+)/(?P<confirm>confirm)/$', views.demenagement, name='demenagement'),
    url('^demenagement/(?P<ptype>adh)/(?P<uid>[0-9]+)/(?P<confirm>confirm)/$', views.demenagement, name='demenagement'),
    url('^delete/(?P<ptype>club)/(?P<uid>[0-9]+)/$', views.delete, name='delete'),
    url('^delete/(?P<ptype>adh)/(?P<uid>[0-9]+)/$', views.delete, name='delete'),
    url('^(?P<ptype>adh)/create/$', views.create, name='create'),
    url('^(?P<ptype>adh)/create/(?P<confirm>confirm)/$$', views.create, name='create'),
    url('^(?P<ptype>club)/create/$', views.create, name='create'),
    url('^(?P<ptype>club)/create/(?P<confirm>confirm)/$$', views.create, name='create'),
    url('^resetpass/(?P<uid>[0-9]+)/$', views.resetpass, name='resetpass'),
]
