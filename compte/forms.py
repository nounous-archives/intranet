# -*- coding: utf-8 -*-


from django import forms
from django.forms import widgets
from django.core.validators import validate_email
from gestion import config
from gestion.config import factures
from gestion.chgpass import check_password
from django.forms.utils import ErrorList
from lc_ldap.attributs import UniquenessError
from lc_ldap.crans_utils import hash_password
import subprocess
import unicodedata
from datetime import date
from dateutil import relativedelta as rdelta
from django.utils.translation import ugettext_lazy as _


class BaseCompteForm(forms.Form):
    nom = forms.CharField(label=_(u'Nom'), max_length=40, required=True)

    def __init__(self, ldap_user=None, *args, **kwargs):
        super(BaseCompteForm, self).__init__(*args, **kwargs)
        if ldap_user:
            self.fields['nom'].initial = ldap_user['nom'][0]
        #self.fields['tel'].initial = ldap_user.get('tel',[''])[0]
    def apply(self, luser):
        """
            Fonction d'application des modifications à l'objet ldap. Doit être appellé après un form.valid
        """
        for field in self.changed_data:
            try:
                luser[field] = unicode(self.cleaned_data[field])
            except ValueError as e:
                elist = self._errors.setdefault(field, ErrorList())
                elist.append(e)
                return False
        return True



class CompteForm(BaseCompteForm):
    prenom = forms.CharField(
        label=_(u'Prénom'),
        max_length=40,
        required=True,
    )
    tel = forms.CharField(
        label=_(u'Téléphone'),
        max_length=15,
        required=True,
    )
    etudes = forms.CharField(
        label=_(u'Etablissement'),
        max_length=100,
        required=True,
    )
    contourneGreylist = forms.BooleanField(
        label=_(u'Contournement du greylisting'),
        required=False,
    )

    def __init__(self, ldap_user=None, *args, **kwargs):
        """
            On initialise le formulaire par rapport aux données du ldap_user
        """
        super(CompteForm, self).__init__(ldap_user, *args, **kwargs)
        if ldap_user:

            self.fields['etudes'].initial = ldap_user.get('etudes', ['Inconnu'])[0]

            self.fields['prenom'].initial = ldap_user['prenom'][0]
            self.fields['tel'].initial = ldap_user['tel'][0]
            self.fields['contourneGreylist'].initial = ldap_user.get('contourneGreylist', [u''])[0]

    def clean_contourneGreylist(self):
        """
            ContourneGreyList est stocké côté LDAP comme "" ou "OK", on convertit donc le bool du formulaire
        """
        if self.cleaned_data['contourneGreylist']:
            return u"OK"
        else:
            return False

    def apply(self, luser):
        """
            Fonction d'application des modifications à l'objet ldap. Doit être appellé après un form.valid
        """
        # On boucle sur les champs "simples"
        # La date de naissance n'est pas un champ ldap
        if 'naissance' in self.changed_data: self.changed_data.remove('naissance')
        for field in self.changed_data:
            try:
                if field == 'contourneGreylist' and not self.cleaned_data[field]:
                    luser[field] = []
                else:
                    luser[field] = unicode(self.cleaned_data[field])
            except ValueError as e:
                elist = self._errors.setdefault(field, ErrorList())
                elist.append(e)
                return False
        return True

class MailForm(forms.Form):
    mailredirect = forms.CharField(label=_(u'Redirection des mails'), max_length=40, required=False)
    def __init__(self, ldap_user, *args, **kwargs):
        super(MailForm, self).__init__(*args, **kwargs)
        redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--read", "--name=%s" % ldap_user['uid'][0]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        mailredirect = redirection_mail.stdout.readlines()
        if mailredirect:
            self.fields['mailredirect'].initial = mailredirect[0]
        else:
            self.fields['mailredirect'].initial = "N/A"

    def clean_mailredirect(self):
        """Nettoie le champ de redirection et vérifie qu'il correspond
        aux critères imposés"""
        mailredirect = self.cleaned_data['mailredirect']

        if not mailredirect.strip():
            return ""

        if "@crans.org" in mailredirect:
            raise forms.ValidationError(_(u"Impossible de rediriger vers une adresse mail crans"))
        try:
            validate_email(mailredirect)
        except forms.ValidationError:
            if not mailredirect.strip('"').strip().startswith('|'):
                raise forms.ValidationError(_(u"Ce champ doit être une adresse mail ou l'execution d'un programme type procmail"))
        return mailredirect

class AliasForm(forms.Form):
    mailAlias = forms.EmailField(label=_(u'Nouvel alias Mail'), max_length=40, required=False)

class BasePassForm(forms.Form):
    newpasswd1 = forms.CharField(
        label=_(u'Nouveau mot de passe'),
        max_length=255,
        widget=widgets.PasswordInput,
        required=False,
    )
    newpasswd2 = forms.CharField(
        label=_(u'Retaper le mot de passe'),
        max_length=255,
        widget=widgets.PasswordInput,
        required=False,
    )

    def clean(self):
        newpasswd1 = self.cleaned_data.get('newpasswd1')
        newpasswd2 = self.cleaned_data.get('newpasswd2')
        check, msg = check_password(newpasswd1)
        if not check:
            raise forms.ValidationError(msg)

        if newpasswd1 != newpasswd2:
            raise forms.ValidationError(_(u"Les mots de passe ne sont pas identiques."))

        return self.cleaned_data

    def apply(self, luser):
        """
            Applique les modifications dans le user ldap
        """
        if self.cleaned_data['newpasswd1']:
            hashedPassword = hash_password(self.cleaned_data['newpasswd1'].encode('utf-8'))
            try:
                luser['userPassword'] = [hashedPassword.decode('ascii')]
                return True

            except EnvironmentError as e:
                errors = self._errors.setdefault("newpasswd2", ErrorList())
                errors.append(e)
                return False

class PassForm(BasePassForm):
    passwdexists = forms.CharField(label=_(u'Ancien mot de passe'), max_length=255, widget=widgets.PasswordInput, required=True)

class SoldeForm(forms.Form):
    montant = forms.DecimalField(
        label=_(u'Montant à ajouter ou supprimer'),
        required=True,
    )
    mode = forms.ChoiceField(
        label=_(u'Mode de réglement'),
        choices=tuple(
            [("", "<choisis un mode de réglement>")] + factures.SOLDE.items()
        ),
        required=True,
    )
    commentaire = forms.CharField(
        label=_(u'Commentaire'),
        max_length=255,
        required=False,
    )

class VenteForm(forms.Form):
    articles = forms.MultipleChoiceField(
        label=_(u'Articles disponibles'),
        choices=tuple(
            (a, b['designation'] + " : " + str(b['pu']) + u"€")\
            for a, b in factures.ITEMS.items()),
        required=True,
    )
    mode = forms.ChoiceField(label=_(u'Mode de réglement'), choices=tuple([("", "<choisis un mode de réglement>")] + factures.VENTE.items()), required=True)
    commentaire = forms.CharField(label=_(u'Commentaire'), max_length=255, required=False)

class AdhesionForm(forms.Form):
    def __init__(self, *args, **kwargs):
        is_cableur = kwargs.pop('is_cableur')
        super(AdhesionForm, self).__init__(*args, **kwargs)
        nbr_mois_label = u"Nombre de mois de connexion, entre 0 et 12"
        if is_cableur:
            nbr_mois_label += u", 0 pour une adhésion seule"
            mode_choix = tuple(factures.VENTE.items())
        else:
            mode_choix = (('solde', u'Vente \xe0 partir du Solde'),)
        self.fields['mode'].empty_label = u"choisir un mode de réglement"
        self.fields['nbrmois'].label = nbr_mois_label
        self.fields['mode'].choices = mode_choix

    nbrmois = forms.IntegerField(min_value=0, max_value=12, required=True)
    mode = forms.ChoiceField(label=_(u'Mode de réglement'), required=True)

class CguForm(forms.Form):
    check = forms.BooleanField(required=True)

class ComptecransForm(forms.Form):
    login = forms.CharField(label=_(u'Login Crans'), required=False)
    def __init__(self, ldap_user=None, ldap_conn=None, ptype=None, *args, **kwargs):
        super(ComptecransForm, self).__init__(*args, **kwargs)
        if ldap_user and ldap_conn and ptype:
            nom = unicode(unicodedata.normalize('NFKD', unicode(ldap_user['nom'][0])).encode('ascii', 'ignore')).lower().replace(' ', '-')
            if ptype != 'club':
                prenom = unicode(unicodedata.normalize('NFKD', unicode(ldap_user['prenom'][0])).encode('ascii', 'ignore')).lower().replace(' ', '-')
                login_crans = nom
                baselogin = nom
                for rang, let in enumerate(prenom):
                    if ldap_conn.search(_(u'uid=%s') % login_crans) == []:
                        break
                    else:
                        login_crans = prenom[:rang] + baselogin
            else:
                login_crans = u'club-' + nom
            self.fields['login'].initial = login_crans

    def apply(self, luser):
        """
            Fonction d'application des modifications à l'objet ldap. Doit être appellé après un form.valid
        """
        if self.cleaned_data['login']:
            try:
                luser.compte(login=unicode(self.cleaned_data['login']))
                return True
            except ValueError as e:
            # Le message d'une ValueError est directement dans e
                elist = self._errors.setdefault('login', ErrorList())
                elist.append(e)
                return False
            except UniquenessError as e:
            # Le message d'une ValueError est directement dans e
                elist = self._errors.setdefault('login', ErrorList())
                elist.append(e)
                return False

        return True
class EmenagementForm(forms.Form):
    chbre = forms.CharField(label=_(u'Chambre de l\'adhérent'), max_length=255, required=True)
    def apply(self, luser, confirm, conn):
        """
            Fonction d'application des modifications à l'objet ldap. Doit être appellé après un form.valid
        """
        chbre = self.cleaned_data['chbre']
        try:
            luser['chbre'] = chbre
            if 'adherent' in luser.get("objectClass", []):
                luser['postalAddress'] = []
            return True
        except ValueError as e:
            self._errors.setdefault('chbre', ErrorList()).append(e)
            return False
        except UniquenessError:
            # La chambre est occupée
            if not confirm:
                # on demande confirmation
                squatteur = conn.search('chbre=%s' % chbre, mode='w')[0]
                if 'adherent' in luser.get("objectClass", []):
                    denomination = _(u"Cette chambre")
                    uid = "aid = %s" % squatteur.get('aid', [''])[0]
                else:
                    denomination = _(u("Ce local"))
                    uid = "cid = %s" % squatteur.get('cid', [''])[0]
                self._errors.setdefault('chbre', ErrorList()).append(
                    _(u"%(denomination)s est déjà occupé(e) par %(squatteur_prenom)s\
                            %(squatteur_nom)s, %(uid)s, corrigez la saisie\
                            ou revalidez pour confirmer.") %
                    {
                        'denomination': denomination,
                        'squatteur_prenom' : squatteur.get('prenom', [''])[0],
                        'squatteur_nom' : squatteur.get('nom', [''])[0],
                        'uid' : uid,
                    }
                )
                return 'confirm' # Ce confirm doit être rattrapé par la view
            else:
                squatteur = conn.search(u'chbre=%s' % chbre, mode='w')[0]
                squatteur['chbre'] = '????' # on vire le squatteur si on a eu une confirmation
                squatteur.history_gen()
                squatteur.save()
                luser['chbre'] = chbre
                return True

class DemenagementForm(EmenagementForm):
    postalAddress = forms.CharField(label=_(u'Adresse :'), max_length=255, required=False)
    postalAddress2 = forms.CharField(label=_(u'Adresse :'), max_length=255, required=False)
    postalAddress3 = forms.CharField(label=_(u'Code Postal :'), max_length=255, required=False)
    postalAddress4 = forms.CharField(label=_(u'Ville :'), max_length=255, required=False)
    chbre = forms.CharField(label=_(u'Chambre de l\'adhérent'), max_length=255, required=False)
    del_machines = forms.BooleanField(label=_(u'Supprimer les machines enregistrées'), required=False)
    def __init__(self, ldap_user=None, *args, **kwargs):
        super(DemenagementForm, self).__init__(*args, **kwargs)
        if ldap_user:
            _postalAddress = ['', '', '', '']

            for i in xrange(len(ldap_user['postalAddress'])):
                _postalAddress[i] = ldap_user['postalAddress'][i]
            self.fields['postalAddress'].initial = _postalAddress[0]
            self.fields['postalAddress2'].initial = _postalAddress[1]
            self.fields['postalAddress3'].initial = _postalAddress[2]
            self.fields['postalAddress4'].initial = _postalAddress[3]

    def clean(self):
        datas = [field for field in self.cleaned_data if self.cleaned_data[field]]
        if "chbre" in datas and len(datas) > 1:
            print self.cleaned_data
            raise forms.ValidationError(_(u"Impossible d'avoir une chambre et une adresse extérieure"))

    def apply(self, luser, confirm, conn):
        """
            Fonction d'application des modifications à l'objet ldap. Doit être appellé après un form.valid
        """
        chbre = self.cleaned_data['chbre']
        # Soit on met un chambre, soit une adresse extérieure
        if chbre:
            # Si on met une chambre, c'est comme pour EmenagementForm
            return super(DemenagementForm, self).apply(luser, confirm, conn)
        else:
            # Sinon, on ajoute l'adresse extérieur
            luser['chbre'] = 'EXT'
            luser['postalAddress'] = [
                self.cleaned_data['postalAddress'],
                self.cleaned_data['postalAddress2'],
                self.cleaned_data['postalAddress3'],
                self.cleaned_data['postalAddress4'],
            ]
            if self.cleaned_data['del_machines']:
                for m in luser.machines():
                    m.delete()
            return True


class NewCompteForm(CompteForm):
    mail = forms.CharField(
        label=_(u'Email'),
        max_length=40,
        validators=[validate_email],
        required=False,
    )
    naissance = forms.DateField(
        label=_(u'Date de naissance'),
        required=True,
    )

    def check_date(self):
        """ Verifie la date de naissance """
        naissance = self.cleaned_data['naissance']
        Now = date.today()
        age = rdelta.relativedelta(Now, naissance).years
        if age < 18:
            return False
        return True


class NewClubForm(BaseCompteForm):
    responsable = forms.IntegerField(label=_(u'Aid du responsable'), required=True)

    def check_date(self):
        return True
