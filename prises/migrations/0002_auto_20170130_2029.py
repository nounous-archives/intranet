# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('prises', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='prise',
            name='has_radius',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prise',
            name='public',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
