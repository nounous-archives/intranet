# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('prises', '0002_auto_20170130_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prise',
            name='public',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
