# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Prise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('batiment', models.CharField(max_length=1)),
                ('chambre', models.CharField(max_length=4)),
                ('prise_crans', models.IntegerField(null=True, blank=True)),
                ('prise_crous', models.IntegerField(null=True, blank=True)),
                ('crans', models.BooleanField(default=True)),
                ('crous', models.BooleanField(default=False)),
                ('commentaire', models.CharField(max_length=1024, null=True, blank=True)),
                ('cablage_effectue', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['batiment', 'chambre'],
                'permissions': (('can_view', 'Peut visualiser les prises'), ('can_change', 'Peut modifier les prises'), ('can_validate', 'Peut valider les c\xe2blages')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PriseAutorise',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aid', models.IntegerField()),
                ('commentaire', models.CharField(max_length=1024, null=True, blank=True)),
                ('prise', models.ForeignKey(to='prises.Prise')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='priseautorise',
            unique_together=set([('prise', 'aid')]),
        ),
    ]
