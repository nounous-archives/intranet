# -*- coding: utf-8 -*-
#
# FORMS.PY -- Formulaires Django pour les prises
#
# Copyright (C) 2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.forms.models import modelformset_factory, ModelForm
from django.forms import Select

from models import Prise, PriseAutorise

ModifPriseFormSet = modelformset_factory(Prise, fields=('crans', 'crous'), extra=0)
ValidPriseFormSet = modelformset_factory(Prise, fields=('cablage_effectue', ), extra=0)

class AutoriseForm(ModelForm):
    """ Formulaire d'édition de couple (user, chambre). Basé sur le modèle, il l'étend légèrement. """
    class Meta:
        model = PriseAutorise
        """user ne doivent pas être modifiés à la main"""
        exclude = ('aid',)

    def __init__(self, *args, **kwargs):
        """ Construit un formulaire. On rajoute un argument facultatif: l'user
            qui édite ce formulaire (pour la création de l'objet).
            On récupère aussi une instance de profil, éventuellement. Si elle
            n'existe pas, c'est que l'on doit créer le profil, le mdp est donc
            obligatoire (sinon non).
            """
        self.aid = kwargs.get('aid',None)
        if kwargs.has_key('aid'):
            del kwargs['aid']
        super(AutoriseForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        """ On applique deux trois modifs avant de réellement sauver: on
        remplace l'user et on redéfinie le mdp si changé"""
        obj = super(AutoriseForm, self).save(commit=False)
        obj.aid=self.aid
        try:
            prise = PriseAutorise.objects.get(aid=self.aid,prise_id=obj.prise.pk)
            prise.commentaire = obj.commentaire
            obj = prise
        except PriseAutorise.DoesNotExist: pass
        if commit:
            return obj.save()
        else:
            return obj
