# -*- coding: utf-8 -*-
#
# PRISES.PY -- Gestion du brassage des prises
#
# Copyright (C) 2009-2010 Antoine Durand-Gasselin
# Copyright (C) 2009-2010 Nicolas Dandrimont
# Authors: Antoine Durand-Gasselin <adg@crans.org>
#          Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

import django.shortcuts
#: Messages d'erreur ou de succès.
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, permission_required

from django.db.utils import ConnectionDoesNotExist
from django.db import IntegrityError, DatabaseError
from django.shortcuts import redirect

#: Redirection vers autre page
from django.http import HttpResponseRedirect
from models import Prise, PriseAutorise
from forms import ModifPriseFormSet, ValidPriseFormSet, AutoriseForm

from intranet import conn_pool

PAGES = [
    ('prise_autorise', 'Mes prises'),
    ('view', 'Modification'),
    ('validate', 'Validation'),
    ]

def get_batiments():
    """Récupère la liste des bâtiments"""
    from django.db import connections

    try:
        cursor = connections['switchs'].cursor()
        batiments = cursor.execute('SELECT DISTINCT batiment FROM prises ORDER BY batiment')
        return set( batiment[0].upper() for batiment in cursor.fetchall())
    except ConnectionDoesNotExist: # Distinct n'est pas implémenté
        return set( v['batiment'].upper() for v in Prise.objects.values('batiment'))

@login_required
def prise_autorise_del(request, pk=None):
    if pk:
        try:
            prise = PriseAutorise.objects.get(pk=pk)
            prise.delete()
        except PriseAutorise.DoesNotExist: pass
    return redirect('prises:prise_autorise')

@login_required
def prise_autorise(request):
    """
        Permet de choisir la liste de prise depuis lesquelles on peut se connecter
    """

    # L'application prise est obsolète, on la cache aux non-MA
    if not request.user.has_perm('auth.crans_cableur'):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")

    aid = conn_pool.get_user(request.user)['aid'][0].value
    chbre = conn_pool.get_user(request.user)['chbre'][0].value
    if request.method == "POST":
        form = AutoriseForm(request.POST, aid=aid)
        if form.is_valid():
            form.save()
    form = AutoriseForm(aid=aid)
    prise_list = [ (str(prise), prise.pk, prise.commentaire) for prise in PriseAutorise.objects.filter(aid=aid)]
    prise_list.sort()
    return django.shortcuts.render(request, "prises/prise_autorise.html", {
        'form': form,
        'user': request.user,
        'prise_list': prise_list,
        'chbre': chbre,
        'PAGES': PAGES,
        'cur_page': 'prise_autorise',
    })

@login_required
def view(request, batiment = None):
    """Affiche les différents batiments et les différentes chambres."""

    # L'application prise est obsolète, on la cache aux non-MA
    if not request.user.has_perm('auth.crans_cableur'):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")

    cur_page = "view"
    batiments = get_batiments()
    if batiment:
        batiment = batiment.lower()
        # On a peut-être des données à récupérer
        if request.method == "POST":
            if request.user.has_perm('prises.can_change'):
                formset = ModifPriseFormSet(request.POST, queryset = Prise.objects.filter(batiment = batiment).order_by('chambre'))
                if formset.is_valid():
                    instances = formset.save(commit=False)
                    for instance in instances:
                        instance.cablage_effectue = not instance.cablage_effectue
                        instance.save()
                    Prise.send_new_cablage_email(instances, request.user)
                else:
                    raise Exception("You're doin it wrong", formset.errors)

        # On affiche les prises du bâtiment
        formset = ModifPriseFormSet(queryset = Prise.objects.filter(batiment = batiment).order_by('chambre'))
        for form in formset.forms:
            form.etage = form.instance.chambre[0]

    l = locals()
    l['PAGES'] = PAGES
    return django.shortcuts.render(request, "prises/prises.html", l)

@login_required
def validate(request, batiment = None):
    """Permet de valider des câblages."""

    # L'application prise est obsolète, on la cache aux non-MA
    if not request.user.has_perm('auth.crans_cableur'):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")

    cur_page = "validate"
    batiments = get_batiments()
    if batiment:
        batiment = batiment.lower()
        if request.method == "POST":
            if request.user.has_perm('prises.can_validate'):
                formset = ValidPriseFormSet(request.POST, queryset = Prise.objects.filter(batiment = batiment, cablage_effectue = False).order_by('crans', 'prise_crous'))
                if formset.is_valid():
                    instances = formset.save()
                    Prise.send_new_validate_email(instances, request.user)
                else:
                    raise Exception("You're doin it wrong", formset.errors)
        # On affiche les prises du bâtiment
        formset = ValidPriseFormSet(queryset = Prise.objects.filter(batiment = batiment, cablage_effectue = False).order_by('crans', 'prise_crous'))
    l = locals()
    l['PAGES'] = PAGES
    return django.shortcuts.render(request, "prises/validate.html", l)
