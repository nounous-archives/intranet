# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Antoine BERNARD
# Authors: Antoine BERNARD <abernard@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
    Formulaire de l'application password_reset
"""

from __future__ import unicode_literals

#: Import des formulaires
from django import forms

#: i18n de l'intranet
from django.utils.translation import ugettext_lazy as _

#: Fonctions de communication avec la base LDAP
from ldap import FILTER_ERROR
from lc_ldap import shortcuts, crans_utils


class EmailForm(forms.Form):
    """
        Formulaire de demande d'une adresse e-mail.
    """
    email = forms.EmailField(
        label=_(u'Adresse e-mail'),
        max_length=254,
        required=True
    )

    def get_user(self):
        """
            Renvoie l'objet LDAP à partir de l'e-mail
        """
        try:
            email = crans_utils.escape(self.cleaned_data['email'])
            conn = shortcuts.lc_ldap_readonly()
            # On cherches les objets LDAP tels que :
            # * le mail est dans le champ `mail` ou `mailExt`
            # * et il a droit de se connecter
            # * et c'est un adhérent
            # * et il a un compte crans
            # * et il n'a pas de droits
            res = conn.search(
                "(&(|(mail=%s)(mailExt=%s))\
                (!(shadowExpire=0))(aid=*)(uid=*)(!(droits=*)))"
                % (email, email)
            )
            return res[0]
        except FILTER_ERROR:
            return []
        except IndexError:
            return []


class UsernameForm(forms.Form):
    """
        Formulaire de demande d'un login Cr@ns.
    """
    username = forms.CharField(
        label=_("Nom d'utilisateur Cr@ns"),
        max_length=254,
        required=True
    )

    def get_user(self):
        """
            Renvoie l'objet LDAP à partir du login
        """
        try:
            login = crans_utils.escape(self.cleaned_data['username'])
            conn = shortcuts.lc_ldap_readonly()
            # On cherches les objets LDAP tels que :
            # * le login est dans le champ `uid`
            # * et il a droit de se connecter
            # * et c'est un adhérent
            # * et il n'a pas de droits
            res = conn.search(
                "(&(uid=%s)(!(shadowExpire=0))(aid=*)(!(droits=*)))" % login
            )
            return res[0]
        except FILTER_ERROR:
            return []
        except IndexError:
            return []
