# -*- encoding: utf-8 -*-
"""
    Views de l'application password_reset
"""

#: Import de settings de l'intranet
from intranet import settings

#: Import de fonctions utiles
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from compte.views import get_admin_luser

#: Import des views Django
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView

#: i18n
from django.utils.translation import ugettext_lazy as _

#: Pour stocker des messages dans la session courante
from django.contrib import messages

#: Import des formulaires
from password_reset.forms import EmailForm, UsernameForm
from compte.forms import BasePassForm

#: Tokenization
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode, int_to_base36
from django.utils.crypto import salted_hmac

#: Décorateurs pour champs sensibles
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator

#TODO:
# * proposer la réinitialisation de mot de passe des clubs
# * mettre le changement de mail sous conditions de mot de passe
# * un script pour envoyer les TODOs par mail

#Copié depuis Django1.11
class MyResetTokenGenerator(PasswordResetTokenGenerator):
    """
        Classe pour la génération d'un token à partir de la
        base LDAP.
    """
    key_salt = "django.contrib.auth.tokens.PasswordResetTokenGenerator"
    secret = settings.SECRET_KEY

    def _make_token_with_timestamp(self, user, timestamp):
        ts_b36 = int_to_base36(timestamp)

        hash = salted_hmac(
            self.key_salt,
            self._make_hash_value(user, timestamp),
            secret=self.secret,
        ).hexdigest()[::2]
        return "%s-%s" % (ts_b36, hash)

    def _make_hash_value(self, user, timestamp):
        # On surchage la méthode pour rechercher les informations
        # dans la base LDAP
        login_timestamp = user.get('derniereConnexion', [''])[0]
        pw = user.get('userPassword', [''])[0]
        return str(user["uidNumber"][0]) + str(pw)\
                + str(login_timestamp) + str(timestamp)

my_token_generator = MyResetTokenGenerator()

class PasswordResetFormView(FormView):
    template_name = "password_reset/password_reset_form.html"
    success_url = reverse_lazy("password_reset:password_reset_done")
    lform = UsernameForm
    eform = EmailForm
    token_generator = my_token_generator

    def form_valid(self, request, form, *args, **kwargs):
        user = form.get_user()
        if user:
            from gestion import mail
            c = {
                'from' : 'cableurs@crans.org',
                'to': str((user["mailExt"] or user["mail"])[0]),
                'name': unicode(user["cn"][0]),
                'protocol': 'https',
                'domain': request.META['HTTP_HOST'],
                'uid': urlsafe_base64_encode(force_bytes(user["aid"][0])),
                'token': self.token_generator.make_token(user),
                'username' : str(user["uid"][0]),
                'mailer' : u"reset_pw",
            }

            # On utilise la class ServerConnection() dans /usr/scripts
            with mail.ServerConnection() as conn_smtp:
                conn_smtp.send_template('password_reset', c)

        return super(PasswordResetFormView, self).form_valid(form, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        req = request.POST

        # On récupere et on rempli le formulaire approrié
        form = self.lform(req) if 'username' in req else self.eform(req)

        if form.is_valid():
            return self.form_valid(request, form, **kwargs)
        else:
            return redirect(self.success_url)

    def get(self, request, next=None, *args, **kwargs):
        # On vérifie que l'utilisateur n'est pas connecté.
        # Sinon, il ne chercherait pas à réinitialiser son mot de passe.
        if not request.user.is_anonymous():
            messages.error(request, _(u"Vous devez vous déconnecter pour accédez à cette page"))
            return redirect(reverse_lazy('index'))
        return render(
            request,
            self.template_name,
            {'lform': self.lform,
             'eform': self.eform,
            },
        )

password_reset = PasswordResetFormView.as_view()

class PasswordResetDoneView(DetailView):
    """
        Vue pour la confirmation d'instructions de réinitialisation
        de mot de passe.

        On confirme toujours sinon quoi on donnerait des informations sur l'existence
        de login ou d'adresses e-mails.
    """
    def get(self, request, next=None, *args, **kwargs):
        if not request.user.is_anonymous():
            messages.error(request, _(u"Vous devez vous déconnecter pour accédez à cette page"))
            return redirect(reverse_lazy('index'))
        return render(
            request,
            "password_reset/password_reset_done.html",
        )

password_reset_done = PasswordResetDoneView.as_view()

class PasswordResetConfirmView(FormView):
    """
        Vue permettant le changement de mot de passe.

        On vérifie la validité du token.
    """
    token_generator = my_token_generator
    form_class = BasePassForm
    success_url = reverse_lazy("password_reset:password_reset_complete")
    template_name = 'password_reset/password_reset_confirm.html'

    def get(self, request, uidb64=None, token=None, *args, **kwargs):
        if not request.user.is_anonymous():
            messages.error(request, _(u"Vous devez vous déconnecter pour accédez à cette page"))
            return redirect(reverse_lazy('index'))

        # On vérifie le token avant de servir la page.
        if not self.is_token_valid(request, uidb64, token):
            return redirect(reverse_lazy("password_reset:password_reset"))

        return render(
            request,
            "password_reset/password_reset_confirm.html",
            {'form' : self.form_class,},
        )

    @method_decorator(sensitive_post_parameters('newpasswd1', 'newpasswd2'))
    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
            View that checks the hash in a password reset link and presents a
            form for entering a new password.
        """
        form = self.form_class(request.POST)

        # On vérifie le token avant de servir la page.
        user = self.is_token_valid(request, uidb64, token)
        if user is None:
            return redirect(reverse_lazy("password_reset:password_reset"))

        # On vérifie la validité du formulaire
        if form.is_valid() and form.apply(user):
            user.history_gen()
            user.save()
            messages.success(request, _(u'Le mot de passe a été réinitialisé'))
            return self.form_valid(form)
        else:
            messages.error(request, _(u'La réinitialisation de mot de passe a échoué'))
            return self.form_invalid(form)

    def is_token_valid(self, request, uidb64, token):
        """
        Check if a given token is valid or not.
        """
        assert uidb64 is not None and token is not None
        try:
            uid = urlsafe_base64_decode(uidb64)
            # On va avoir besoin d'écrire pour changer le mot de passe.
            user = get_admin_luser(request, uid, 'adh', mode='w')
        except (TypeError, ValueError, OverflowError):
            user = None

        # Si `user` existe et le `token` n'a pas expiré.
        if user is not None and self.token_generator.check_token(user, token):
            return user
        # Sinon
        messages.error(request, _(u"Le token de réinitialisation n'est plus valide."))
        return None

password_reset_confirm = PasswordResetConfirmView.as_view()

class PasswordResetCompleteView(DetailView):
    """
        Vue finale.
    """
    def get(self, request, *args, **kwargs):
        if not request.user.is_anonymous():
            messages.error(request, _(u"Vous devez vous déconnecter pour accédez à cette page"))
            return redirect(reverse_lazy('index'))
        return render(
            request,
            "password_reset/password_reset_complete.html",
        )
password_reset_complete = PasswordResetCompleteView.as_view()
