# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Transaction

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('idTransaction', 'uid', 'created', 'montant', 'valid', )
