# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('solde', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 21, 13, 8, 57, 964356), help_text=b'Date de cr\xc3\xa9ation', auto_now_add=True),
            preserve_default=False,
        ),
    ]
