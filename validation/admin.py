#~*~ coding: utf-8 ~*~
from django.contrib import admin
from django import forms
import models

class ConfirmActionAdmin(admin.ModelAdmin):
    list_display = ('view_name', 'pk', 'created', 'triggered', 'data', )

admin.site.register(models.ConfirmAction, ConfirmActionAdmin)

