# -*- coding: utf-8 -*-
from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.index, name="index"),
    url('^edit/$', views.edit, name="edit"),
    url('^delete/$', views.delete, name="delete"),
    url('^book/$', views.book, name="book"),
    url('^book/download$', views.book, {'download': True}, name="book_download",),
    url('^history/$', views.history, name="history"),
    url('^history/(?P<page>[0-9]+)$', views.history),
]
