#  -*- coding: utf-8 -*-
"""Formulaire pour les adhérents, permettant d'ajouter
des infos sur sa page perso."""

from django import forms
from models import PagePerso
from django.utils.translation import ugettext_lazy as _

class PagePersoForm(forms.ModelForm):
    """ModelForm proposant un formulaire basé sur la classe
    PagePerso. C'est du vieux django, faudra mettre à jour."""
    class Meta:
        model = PagePerso
        fields = ['nom_site', 'slogan', 'logo']


class SqlPasswordForm(forms.Form):
    password = forms.CharField(
        label="mot de passe",
        max_length=256,
        widget=forms.PasswordInput,
        required=True
    )
    password_confirmation = forms.CharField(
        label="confirmation",
        max_length=256,
        widget=forms.PasswordInput,
        required=True
    )

    def clean_password(self):
        if len(self.cleaned_data["password"]) < 8:
            raise forms.ValidationError(_(u"Le mot de passe doit faire au moins 8 caractères."))
        return self.cleaned_data["password"]

    def clean(self):
        if "password" in self.cleaned_data:
            if self.cleaned_data["password"] != self.cleaned_data.get("password_confirmation"):
                raise forms.ValidationError(_(u"Les mots de passe ne sont pas identiques."))
