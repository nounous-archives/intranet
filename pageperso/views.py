# -*- coding: utf-8 -*-
# Application pour permettre aux adhérents de lister leur page perso
# Gabriel Détraz <detraz@crans.org>, Hamza Dely <dely@crans.org>

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.core.exceptions import PermissionDenied
from models import PagePerso
from django.utils.translation import ugettext_lazy as _

from intranet import conn_pool
from gestion.mysql import make_cursor
from gestion import secrets_new as secrets

try:
    cursor = make_cursor("intranet_cursor", secrets.get("intranet_mysql_admin"))
except (secrets.SecretNotFound, secrets.SecretForbidden):
    cursor = None

# intranet_mysql_admin
from forms import PagePersoForm, SqlPasswordForm

def test_backticks(name):
    if '`' in name:
        raise ValueError("%r should not contain '`'")
    return name

def managed_logins(user):
    """Renvoie la liste des logins gérés par ``user``. Travaille avec un générateur
    pour ne pas vraiment calculer la liste des clubs, si non nécessaire."""
    yield user.username
    luser = conn_pool.get_user(user)
    if not user:
        return
    for club in luser.clubs():
        for uid in club.get('uid', []):
            yield uid.value

def get_login(request, login):
    """Renvoie le login présumé de la page perso. Et vérifie que l'utilisateur
    courant a bien le droit de faire des modifs dessus"""
    login = login or request.user.username
    if login in managed_logins(request.user) or request.user.has_perm('crans_nounou'):
        return login
    raise PermissionDenied
    
# Vue d'affichage du formulaire d'indexation de la pageperso
@login_required
@require_http_methods(["GET", "POST"])
def pageperso(request, login=None):
    login = get_login(request, login)
    result = PagePerso.objects.filter(login=login)
    if request.method == "POST":
        if result.exists():
            form = PagePersoForm(request.POST, instance=result[0])
        else:
            form = PagePersoForm(request.POST)
        if form.is_valid():
            if result.exists():
                form.save()
            else:
                new_page = form.save(commit=False)
                new_page.login = login
                new_page.save()
            labelperso = _(u"Modifier l'affichage de ma page perso")
            deref = True
        else:
            labelperso = _(u"Référencer ma page perso")
            deref = False

    # Si c'est pas un post, on renvoie le formulaire prérempli (si deja une page)
    elif request.method == "GET":
        if result.exists():
            res = result[0]
            initial_data = {
                'nom_site': res.nom_site,
                'slogan': res.slogan,
            }
            form = PagePersoForm(instance=result[0])
            labelperso = _(u"Modifier l'affichage de ma page perso")
            deref = True
        # Si la page existe pas, le champ est vierge
        else:
            form = PagePersoForm()
            labelperso = _(u"Référencer ma page perso")
            deref = False
        if cursor:
            mysql_error = False
            with cursor() as cur:
                if cur.execute(
                    "SELECT User From mysql.user WHERE User = %s",
                    (login, )
                ):
                    # one user found, clean cursor
                    cur.fetchone()
                    mysql_exists = True
                    if not cur.execute(
                        (
                            "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA "
                            "WHERE SCHEMA_NAME = %s"
                        ),
                        (login, )
                    ):
                        cur.execute(
                            (
                                "CREATE DATABASE `%s` CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
                            ) % test_backticks(login)
                        )
                        cur.execute(
                            (
                                "GRANT ALL PRIVILEGES ON `%s`.* TO %%s@'localhost'"
                            ) % test_backticks(login),
                            (login, )
                        )
        else:
            mysql_error = True
    logins = managed_logins(request.user)
    return render(request, "pageperso/affichage.html",
        locals()
    )

@login_required
def deref(request, login=None):
    login = get_login(request, login)
    result = PagePerso.objects.filter(login=login)
    result.delete()
    return redirect("pageperso:pageperso", login=login)


@login_required
@require_http_methods(["GET", "POST"])
def create_database(request, login=None):
    login = get_login(request, login)
    if cursor is None:
        return redirect("pageperso:pageperso", login=login)
    if request.method == "POST":
        form = SqlPasswordForm(request.POST)
        if form.is_valid():
            with cursor() as cur:
                cur.execute(
                    (
                        "CREATE DATABASE IF NOT EXISTS `%s` CHARACTER SET utf8 "
                        "COLLATE utf8_unicode_ci;"
                    ) % test_backticks(login)
                )
                cur.execute(
                    (
                        "GRANT ALL PRIVILEGES ON `%s`.* TO %%s@'localhost' IDENTIFIED BY %%s"
                    ) % test_backticks(login),
                    (login, form.cleaned_data["password"])
                )
            return redirect("pageperso:pageperso", login=login)
    elif request.method == "GET":
        form = SqlPasswordForm()
    return render(request, "pageperso/createdb.html", {'form': form, 'login': login, })


@login_required
@require_http_methods(["GET", "POST"])
def reset_db_password(request, login=None):
    login = get_login(request, login)
    if cursor is None:
        return redirect("pageperso:pageperso", login=login)
    if request.method == "POST":
        form = SqlPasswordForm(request.POST)
        if form.is_valid():
            with cursor() as cur:
                if cur.execute(
                    "SELECT User From mysql.user WHERE User = %s",
                    (login, )
                ):
                    cur.fetchone()
                    cur.execute(
                        "SET PASSWORD FOR %s@'localhost' = PASSWORD(%s);",
                        (login, form.cleaned_data["password"],)
                    )
            return redirect("pageperso:pageperso", login=login)
    elif request.method == "GET":
        form = SqlPasswordForm()
    return render(request, "pageperso/createdb.html", {'form': form, 'login': login})


@login_required
@require_http_methods(["GET", "POST"])
def delete_database(request, login=None):
    login = get_login(request, login)
    if cursor is None:
        return redirect("pageperso:pageperso", login=login)
    if request.method == "POST":
        if request.POST.get("confirm") == "yes":
            with cursor() as cur:
                cur.execute("DROP DATABASE IF EXISTS `%s`" % test_backticks(login))
                cur.execute("DROP USER %s@'localhost'", (login, ))
        return redirect("pageperso:pageperso", login=login)
    return render(request, "pageperso/deletedb.html", {'login': login})
