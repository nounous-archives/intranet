# -*- coding: utf-8 -*-
from django.conf.urls import url, include

import views

urlpatterns = [
    url('^$', views.pageperso, name='pageperso'),
    url('^deref', views.deref, name='deref'),
    url('^createdb', views.create_database, name='create_database'),
    url('^resetdbpw', views.reset_db_password, name='reset_db_password'),
    url('^deletedb', views.delete_database, name='delete_database'),
]

# Spécifier en plus un utilisateur pour qui éditer
urlpatterns = [url(r'^(?:(?P<login>[^/]*)/)?', include(urlpatterns))]
