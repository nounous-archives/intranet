# -*- coding: utf-8 -*-
"""Modèle basique pour la gestion des pages persos"""

from django.db import models

class PagePerso(models.Model):
    nom_site = models.CharField(max_length=50, verbose_name='nom du site')
    slogan = models.CharField(max_length=255)
    login = models.CharField(max_length=255, unique=True, primary_key=True)
    logo = models.CharField(max_length=50, blank=True)
    class Meta:
        verbose_name = 'page perso'

