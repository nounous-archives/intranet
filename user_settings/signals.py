# -*- coding: utf-8 -*-
from django.dispatch import receiver
from django.contrib.auth import user_logged_in
from django.utils import translation

@receiver(user_logged_in)
def restore_user_settings(sender, request, user, **kwargs):
    try:
        request.session[translation.LANGUAGE_SESSION_KEY] = user.settings.langue
    except AttributeError:
        pass
