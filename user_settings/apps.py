# -*- coding: utf-8 -*-
from django.apps import AppConfig

class UserSettingsConfig(AppConfig):
    name = 'user_settings'
    verbose_name = 'User Settings'

    def ready(self):
        import signals
