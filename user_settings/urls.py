# -*- coding: utf-8 -*-
from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.UserSettingsUpdateOrCreateView.as_view(), name='edit'),
]
