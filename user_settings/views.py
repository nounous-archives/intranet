# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic.edit import UpdateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.utils import translation

from models import UserSettings

class UserSettingsUpdateOrCreateView(UpdateView):
    template_name = 'edit.html'
    model = UserSettings
    fields = ['langue']
    success_url = reverse_lazy('index')

    def get_object(self):
        instance, created = UserSettings.objects.get_or_create(user=self.request.user)
        return instance

    def form_valid(self, form, *args, **kwargs):
        messages.success(self.request, _(u'Vos préférences ont bien été sauvegardées'))
        self.request.session[translation.LANGUAGE_SESSION_KEY] = form.cleaned_data['langue']
        return super(UserSettingsUpdateOrCreateView, self).form_valid(form, *args, **kwargs)


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserSettingsUpdateOrCreateView, self).dispatch(*args, **kwargs)

