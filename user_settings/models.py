# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from intranet.settings import LANGUAGES

class UserSettings(models.Model):
    user = models.OneToOneField(User, related_name='settings')
    langue = models.CharField(_(u"langue"), max_length=2, choices=LANGUAGES)
