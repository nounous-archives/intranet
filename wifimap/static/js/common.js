/* 
 *  Petite compilation de fonctions utiles
 *  Copyright (C) 2012 Daniel STAN
 *  Authors: Daniel STAN <daniel.stan@crans.org>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function getElementsByClassName(obj, className) {
    className = new RegExp(' *' + className + ' ?');
    res = [];
    function aux(html) {
        if( html.nodeType != html.ELEMENT_NODE) return;
        if( html.className && html.className.match(className) )
            res.push(html);
        for( var child=html.firstChild; child; child=child.nextSibling)
        {
            aux(child)
        }
    }
    aux(obj);
    return res;
}

function getInnerText(obj) {
    if(!obj) return '';
    if( obj.nodeType != obj.ELEMENT_NODE) return obj.nodeValue;
    var res = '';
    for(var child=obj.firstChild; child; child=child.nextSibling)
        res += getInnerText(child);
    return res;
}

function setInnerText(obj,txt) {
    while(obj.firstChild)
        obj.removeChild(obj.firstChild);
    obj.appendChild(obj.ownerDocument.createTextNode(txt));
}

//Some format
var format_uptime_cases = [['seconde',60],['minute',60],['heure',24],
    ['jour',365],['an',Infinity]];
function format_uptime(reste) {
    var str = '';
    var tab = format_uptime_cases;
    var i = 0;
    while( reste > 0 ) {
        lab = reste%tab[i][1];
        reste = Math.floor(reste/tab[i][1]);
        if(lab > 0)
            str = lab + ' ' + tab[i][0] + (lab > 1?'s':'') + ' ' + str;
        i++
    }
    return str;
}

var templates = {
   
    'enable': function (obj) {
        //Retire la class "template" d'un objet html (pour affichage)
        obj.className = obj.className.replace(/ *template ?/,'')
    },

    'fillSpan': function (obj,text) {
        //Remplit le premier sous-objet à la classe 'content' avec text
        //Et l'affiche, ou lui même si pas de classe content
        var content = getElementsByClassName(obj,'content');
        if( content[0])
            content = content[0];
        else
            content = obj;
        setInnerText(content,text);
        templates.enable(obj);
    },

    'fillSpanClass': function (obj,cl,text) {
        //trouve le premier objet avec la class donnée et applique fillSpan
        templates.fillSpan(getElementsByClassName(obj,cl)[0],text);
    },

    'getTemplate': function (obj,cl) {
        //Cherche le premier objet avec cl pour class et template
        var tpl = getElementsByClassName(obj,cl);
        for( var i in tpl) {
            if( tpl[i].className.match(/ *template ?/))
                return tpl[i];
        }
        return undefined;
    },
    'getInstance': function (obj,cl) {
        //Copie à la suite et retourne le premier objet de classe donné et
        //le retourne
        var tpl = templates.getTemplate(obj,cl);
        var actu = tpl.cloneNode(true);
        tpl.parentNode.insertBefore(actu,tpl);
        templates.enable(actu);
        return actu;
    }

};
