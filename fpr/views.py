# -*- coding: utf-8 -*


from django.shortcuts import render

from lc_ldap import shortcuts
from lc_ldap.crans_utils import escape

import base64
import hashlib

# Use case 1 (ssh vers zamok):
# "ECDSA key fingerprint is SHA256:EJib/9V3VjjkUX6w8bk2zt8BKxmG0JK6D+PDEyLtNb0."
#
# Use case 2 (ssh vers zamok, known as hex format for md5):
# "The fingerprint for the ECDSA key sent by the remote host is
# 61:0d:57:e1:c8:58:93:c5:2d:75:a2:b5:b4:67:97:e2."
# Or: "ECDSA key fingerprint is 61:0d:57:e1:c8:58:93:c5:2d:75:a2:b5:b4:67:97:e2."
#
# Use case 3 (ce qui est dans ldap, aka la clé publique, la partie du milieu se
# hash bien sha256 ou md5):
# sshFingerprint: ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFBDTqRbMcXkQ+VMf
# Nx4Ph269zPQdOEsbvxbrOBjQrMMIM3ouOP+PxxCrKbM3dAVBDDnrSX10QaEf/1xCjM2xdw= root@zamokv5
#
# Use case 4 (ce qui est dans le dns):
# $ host -t sshfp zamok.crans.org
# zamok.crans.org has SSHFP record 3 2 10989BFFD5775638E4517EB0F1B936CEDF012B1986D092BA0FE3C313 22ED35BD
# zamok.crans.org has SSHFP record 1 1 8CFD4660595B1090BCF1D9E8B3660F70CBB89B38
# zamok.crans.org has SSHFP record 3 1 D322279C95D9E8FD0A2DE90F258C900C1716D8F3
# zamok.crans.org has SSHFP record 1 2 5E7BB20A70AD4DB823E4FC63233BC2E138527410ABB4620273A63A43 E74A221E
# La première ligne correspond à ecdsa (3) en sha256 (2)

conn = shortcuts.lc_ldap_readonly()
def index(request):
    host = request.GET.get('host', u'') or u'zamok.crans.org'
    host = host[:50]
    if not host.endswith('.crans.org'):
        host += '.crans.org'
    machines = conn.search(u'(|(host=%(h)s)(hostAlias=%(h)s))' % {'h': escape(host)})
    fpr_list = list()
    for machine in machines:
        for fpr in machine['sshFingerprint']:
            fpr_list.append({
                'type': nice_type(fpr['type']),
                'hash': sha256_format(fpr['key']),
            })
            fpr_list.append({
                'type': nice_type(fpr['type']),
                'hash': md5_format(fpr['key']),
            })
    return render(request, 'fpr/index.html', {
        'fpr_list': fpr_list,
        'host': host,
    })

def md5_format(pub_key):
    """Return md5 fingerprint, as shown by ssh"""
    pub_key = base64.decodestring(pub_key)
    fpr = hashlib.md5(pub_key).hexdigest()
    return ':'.join(fpr[2*i] + fpr[2*i+1] for i in range(len(fpr)/2))

def sha256_format(pub_key):
    """Return sha256 fingerprint, as shown by ssh"""
    pub_key = base64.decodestring(pub_key)
    fpr = base64.encodestring(hashlib.sha256(pub_key).digest())
    # some clean up
    fpr = fpr.replace('=','').strip()
    return "SHA256:" + fpr

def nice_type(t):
    """C'est un chic type lui.
    Renvoie le nom commun affiché par ssh au sujet d'un type de clé, vous allez
    voir, c'est très con."""
    if 'rsa' in t:
        return "RSA"
    if 'ed25519' in t:
        return "ED25519"
    if 'ecdsa' in t:
        return 'ECDSA'
    return t
