# -*- coding: utf-8 -*-
# Django settings for intranet project.

import os
import sys
import socket
from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import django
import gestion.secrets_new as secrets
import conn_pool

""" Liste d'applications désactivées """
DISABLED_APPS = ['dummy', ]

# TODO Section d'insultes pour obsolescence
try:
    import settings_local
    raise Exception("Présence d'un settings_local, merci de relire le README "
        '(section "Vieille installation de test")')
except ImportError:
    pass

if not os.getenv('DBG_INTRANET'):
    if socket.gethostname() != 'o2':
        raise Exception("DBG_INTRANET absent et nous ne sommes pas en prod "
            "sur o2. Qu'est-ce que ça veut dire ?")
    # L'url de l'intranet
    ROOT_URL = "https://intranet.crans.org/"
    DEBUG = False
    BASE_LDAP_TEST = False
else:
    ROOT_URL = os.getenv('DBG_INTRANET')
    DEBUG = True
    # TODO Virer mention de la base de test
    BASE_LDAP_TEST = True

# Faut-il utiliser le CAS pour s'authentifier
CAS_ENABLED = os.getenv('DBG_CAS') not in ['', '0']

# À qui faut-il envoyer les mails de câblage
CABLAGE_MAIL_DEST = ['respbats@crans.org', ]

if not DEBUG:
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Où stocker les fichiers static collectés
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, 'var/static'))

SESSION_COOKIE_AGE = 900

SESSION_SAVE_EVERY_REQUEST = True

EMAIL_SUBJECT_PREFIX = "[Intranet2 Cr@ns] "

# Nécessaire pour timezone.now() renvoie un datetime timezoné
USE_TZ = True

ADMINS = (
    ('Intranet', 'root@crans.org'),
)

ALLOWED_HOSTS = [
    'intranet2.crans.org',
    'intranet.crans.org',
    'intranet-dev.crans.org',
    'intranet-apprentis.crans.org',
    'vo.crans.org',
]


ATOMIC_REQUESTS = True

MANAGERS = ADMINS

FIXTURE_DIRS=(os.path.join(BASE_DIR, 'fixtures/'),)

DB_HOST = os.getenv('DBG_DJANGO_DB', 'pgsql.v4.adm.crans.org')
# NB: DBG_DJANGO_DB peut être défini mais vide (base SQLite)
if DB_HOST:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'django',
            'HOST': DB_HOST,
            'USER': 'crans',
        },
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, "default.sqlite"),
        },
    }


STATIC_URL = '/static/'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

LANGUAGES = [
    ('fr', _(u'Français')),
    ('en', _(u'Anglais')),
    ('es', _(u'Espagnol')),
]
DEFAULT_LANGUAGE = 1

# Formate les dates en fonction de la localisation
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
# TODO: deprecated ?
MEDIA_ROOT = BASE_DIR

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ROOT_URL + 'media/'

# Make this unique, and don't share it with anybody.
if DEBUG:
    SECRET_KEY = "sYjlDBZUBSMnk"
else: # Ne marchera que sur o2
    SECRET_KEY = secrets.get('django_secret_key')

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'reversion.middleware.RevisionMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TPL_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.template.context_processors.debug",
    "django.template.context_processors.i18n",
    "django.template.context_processors.media",
    "django.contrib.messages.context_processors.messages",
    "django.template.context_processors.request",
)

TPL_CONTEXT_PROCESSORS += (
    'intranet.custom_context_processor.app_list',
    'intranet.custom_context_processor.global_message',
)
if os.getenv('DBG_LDAP'):
    TPL_CONTEXT_PROCESSORS += (
        'intranet.custom_context_processor.password_hint',
    )


TEMPLATES = [
{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(BASE_DIR,'templates')],
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': TPL_CONTEXT_PROCESSORS,
        'debug' : DEBUG,
        },
},]

ROOT_URLCONF = 'intranet.urls'

LOGIN_URL = "/login"
LOGIN_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

CAS_SERVER_URL = 'https://cas.crans.org/'
CAS_VERSION = '3'

# En dehors d'un usage personnel, on se permet d'activer le backend LDAP
# (reste à voir si on se connecte à la vraie ou à la base de test)
if CAS_ENABLED:
    AUTHENTICATION_BACKENDS.append('django_cas_ng.backends.CASBackend')
AUTHENTICATION_BACKENDS.append('intranet.login.LDAPUserBackend')

# Le nom de l'appli prises est utilisé pour les reverse URL.
#  Si jamais on a envie de le changer, pour que ce soit plus simple,
#  on le place dans une variable ici.
APP_PRISES_NAME = "prises"

CABLAGE_MAIL_FROM = u'"L\'intranet du Cr@ns" <intranet-bugreport@lists.crans.org>'

CATEGORY_LABELS = {
  'Administration': _(u'Administration'),
  'Services': _(u'Services'),
  'Divers': _(u'Divers'),
}

INTRANET_APPS = (
    {
        'name': 'dummy',
        'title': _(u'Application factice vide pour tests'),
        'category' :'Beta',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti'),
    },
    {
        'name': APP_PRISES_NAME,
        'category':'Administration',
        'label': _(u'Prises réseau'),
        'test': lambda u: u.has_perm('prises.can_view') or u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_cableur'),
    },
    {
        'name': 'impressions',
        'category': 'Services',
        'label': _(u'Impressions'),
        'help':'https://wiki.crans.org/VieCrans/ImpressionReseau',
        'title': _(u"Service d'impression sur l'imprimante Crans"),
    },
    {
        'name': 'digicode',
        'category': 'Services',
        'label': _(u'Gestion des digicodes'),
        'title': _(u"Gestion des codes d'accès au local d'impression (4J)"),
    },
    {
        'name': 'wifimap',
        'title': _(u'Carte des positions et état des bornes wifi Cr@ns'),
        'category': 'Services',
        'label': _(u'Carte WiFi'),
    },
    {
        'name': 'tv',
        'module': 'stream',
        'title': _(u'Télévison et radio'),
        'category':'Services',
        'label': _(u'TV'),
        'help':'https://wiki.crans.org/TvReseau',
    },
    {
        'name': 'wiki',
        'module': 'association_wiki',
        'category': 'Administration',
        'label': _(u'Compte WiKi'),
        'title': _(u'Associer son compte crans avec le wiki du crans'),
        'help':'https://wiki.crans.org'
    },
    {
        'name': 'compte',
        'title': _(u'Gestion de compte'),
        'category': 'Administration',
        'label': _(u'Mon Compte'),
        'help': 'https://wiki.crans.org/VieCrans/GestionCompte/'
    },
    {
        'name': 'cablage',
        'category': 'Administration',
        'label': _(u'Câblage'),
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_cableur'),
    },
    {
        'name': 'pageperso',
        'title': _(u'Gestion page perso'),
        'category': 'Administration',
        'label': _(u'Page Perso'),
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/#Comment_cr.2BAOk-er_sa_page_personnelle_.3F',
    },
    {
        'name' : 'solde',
        'title' : _(u'Consulter son solde et recharger son compte Crans'),
        'category' : 'Administration',
        'label' : _(u'Mon Solde'),
        'help': 'https://wiki.crans.org/CransPratique/SoldeImpression',
    },
    {
        'name': 'quota',
        'title': _(u'Gestion des Quotas (upload, dossier)'),
        'category': 'Administration',
        'label': _(u'Quotas'),
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/GestionQuota',
    },
    {
        'name': 'machines',
        'category': 'Administration',
        'label': _(u'Gestion des machines'),
        'test': lambda u: (u.groups.filter(name='crans_paiement_ok') or conn_pool.get_user(u).machines()),
        'title': _(u'Créer, modifier ou supprimer des machines'),
    },
    {
        'name': 'factures',
        'category': 'Administration',
        'label': _(u'Mes factures'),
        'title': _(u"Affichage des factures"),
    },
    {
        'name': 'validation',
        'category': 'Administration',
        'label': _(u'Validation'),
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_cableur'),
    },
    {
        'name': 'autoconf',
        'category': 'Administration',
        'label': 'autoconf',
        'test': lambda _: False, # Personne n'a besoin de voir cette appli
    },
    {
        'name': 'club',
        'category': 'Administration',
        'label': _(u'Gestion des clubs'),
        'test': lambda u: u.groups.filter(name='crans_respo_club'),
    },
    {
        'name': 'user_settings',
        'label': _(u'Préférences'),
        'category' :'Administration',
        'title' : _(u'Réglages des préférences'),
        'test': lambda u: True,
    },
    {
        'name': 'fpr',
        'label': _(u'Empreintes SSH'),
        'category' :'Services',
        'title' : _(u'Affichage des empreintes SSH des serveurs'),
        'test': lambda u: True,
    },
    {
        'name': 'password_reset',
        'label': _(u'password_reset'),
        'category' :'Administration',
        'title' : _(u'password_reset'),
        'test': lambda _: False,
    },
)

INTRANET_APPS = tuple([ i for i in INTRANET_APPS if i['name'] not in
    DISABLED_APPS])

REDIRECT_SERVICES = (
    {
        'url': 'https://gitlab.crans.org/',
        'category': 'Divers',
        'label': _(u'GitLab'),
        'pic' : 'icone_gitlab',
        'test': lambda _: True,
        'title': _(u'Interface web pour gérer des dépôts Git'),
        'help':'https://wiki.crans.org/CransTechnique/GitLab',
    },
    {
        'url': 'https://roundcube.crans.org',
        'category': 'Services',
        'label': _(u'Webmail'),
        'pic' : 'icone_roundcube',
        'test': lambda u: u.groups.filter(name='crans_adherent') or u.groups.filter(name='crans_club'),
        'title': _(u'Interface web pour vos mails crans'),
        'help':'https://wiki.crans.org/VieCrans/LesMails',
    },
    {
        'url': 'https://webnews.crans.org/newsgroups.php?cas=crans.org',
        'category': 'Services',
        'label': _(u'Webnews'),
        'pic' : 'icone_webnews',
        'title': _(u'Interface web pour les newsgroups (les forums) crans'),
        'help':'https://wiki.crans.org/VieCrans/ForumNews',
    },
    {
        'url': 'http://webirc.crans.org',
        'category': 'Services',
        'label': _(u'Webirc'),
        'pic' : 'icone_irc',
        'test': lambda _: True,
        'title': _(u'Interface web pour les canaux de discussion du crans'),
        'help': 'https://wiki.crans.org/VieCrans/UtiliserIrc',
    },
    {
        'url': 'https://owncloud.crans.org/index.php?app=user_cas',
        'category': 'Services',
        'label': _(u'ownCloud'),
        'pic': 'icone_owncloud',
        'test': lambda u: u.groups.filter(name='crans_adherent') or u.groups.filter(name='crans_club'),
        'title': _(u'Interface web pour stocker des fichiers à la dropbox'),
        'help': 'https://wiki.crans.org/VieCrans/Owncloud',
    },
    {
        'url': 'https://pad.crans.org/',
        'category': 'Divers',
        'label': _(u'Etherpad'),
        'test': lambda _: True,
        'pic': 'icone_etherpad',
        'title': _(u"Une interface web pour faire de l'édition collaborative de texte"),
    },
    {
        'url': 'https://zero.crans.org/',
        'category': 'Divers',
        'label': _(u'ZeroBin'),
        'test': lambda _: True,
        'pic': 'icone_zerobin',
        'title': _(u"Un outil minimaliste pour partager du texte"),
    },
    {
        'url': 'https://phabricator.crans.org',
        'category': 'Divers',
        'label': _(u'Phabricator'),
        'test': lambda _: True,
        'pic': 'icone_phabricator',
        'title': _(u"Un gestionnaire de tâches ouvert à tous"),
    },
    {
        'url': 'https://ethercalc.crans.org/',
        'category': 'Divers',
        'label': _(u'Ethercalc'),
        'test': lambda _: True,
        'pic': 'icone_ethercalc',
        'title': _(u"Une interface web pour faire de l'édition collaborative de tableur"),
    },
    {
        'url': 'https://limesurvey.crans.org/',
        'category': 'Services',
        'label': _(u'Limesurvey'),
        'test': lambda _: True,
        'pic': 'icone_limesurvey',
        'title': _(u"Une interface web pour l'élaboration d'enquêtes et questionnaire"),
    },
)

#REDIRECT_SERVICES = tuple([i.update({'name' : 'redirect/%s' % i['url']}) or i for i in REDIRECT_SERVICES ])
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    #'south',
    'reversion',
    'custom_shortcuts',
    'printer',
    'intranet',
) + tuple( app.get('module', app['name']) for app in INTRANET_APPS )

if CAS_ENABLED:
    INSTALLED_APPS = INSTALLED_APPS + ('django_cas_ng',)

# Config de l'app d'impressions
MAX_PRINTFILE_SIZE=150 * 1024 * 1024 # 25Mio

###############################################################################
# Django >= 1.6
###############################################################################
CSRF_HTTP_ONLY = True


###############################################################################
# Django >= 1.7
###############################################################################
SESSION_COOKIE_SECURE = not DEBUG

SILENCED_SYSTEM_CHECKS = ["1_6.W001"]

###############################################################################
# Performance
###############################################################################
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

#Message global à afficher sur le site
GLOBAL_MESSAGES = None
