# -*- coding: utf-8 -*-
#
# ACCUEIL.PY
#
# Copyright (C) 2010 Antoine Durand-Gasselin
# Author: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.debug import sensitive_variables, sensitive_post_parameters

import login
import conn_pool
from forms import PassForm

@login_required
def accueil(request):
    '''Affiche toutes les applications disponibles en fonction des
    droits de la personne connectée.'''
    return render(request, "accueil.html")

@sensitive_post_parameters('passwd')
@permission_required('auth.crans_ma')
def surlogin(request):
    username = request.user.username.split('@')[0]
    if request.method == 'POST':
        form = PassForm(username, request.POST)
        if form.is_valid():
            _surlog(request, True)
            if 'next' in request.GET:
                return redirect(request.GET['next'])
            return render(request, "surlogin.html", {
                'message': _(u'Vous êtes désormais connecté avec vos droits Crans !'),
            })
    else:
        form = PassForm(username)
    return render(request, "surlogin.html", {
        'message': _(u'Veuillez vous authentifier pour récupérer vos droits.'),
        'form': form,
    })

@permission_required('auth.crans_ma')
def surlogout(request):
    _surlog(request, False)
    if 'next' in request.GET:
        return redirect(request.GET['next'])
    return render(request, "surlogin.html", {
        'message': _(u'Vous avez renoncé à vos droits.'),
    })

def _surlog(request, allow=True):
    user = request.user
    luser = conn_pool.get_user(request.user)
    login.refresh_droits(user, luser, allow_staff=allow)

