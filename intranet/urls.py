# -*- encoding: utf-8 -*-

from django.conf.urls import include, url
from intranet import settings
import django.contrib.auth.views
from django.views.decorators.debug import sensitive_post_parameters
from intranet.views import accueil, surlogin, surlogout

from django.contrib import admin
admin.autodiscover()

if settings.CAS_ENABLED:
    from django_cas_ng.signals import cas_user_authenticated
    from intranet.login import post_cas_login
    cas_user_authenticated.connect(post_cas_login)
    from django_cas_ng.views import login, logout
    login_url = url(r'^login$', login, name="login")
    logout_url = url(r'^logout$', logout, name="logout")
else:
    login_view = django.contrib.auth.views.login
    login_view = sensitive_post_parameters('password')(login_view)
    login_url = url('^login', login_view, {'template_name': 'login.html'}, name="login")
    #login_url = url('^login', protect(django.contrib.auth.views.login), {'template_name': 'login.html'}, name="login")
    logout_url = url('^logout', django.contrib.auth.views.logout_then_login, name ="logout")

urlpatterns = [
    # Les pages existantes
    url('^$', accueil, name='index'),
    url('^surlogin$', surlogin, name='surlogin'),
    url('^surlogout$', surlogout, name='surlogout'),
    # Pages de login
    login_url,
    logout_url,
    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]

for app in settings.INTRANET_APPS:
    app_name = app["name"]
    module = app.get('module', app_name)
    urlpatterns += [url('^%s/' % app_name, include('%s.urls' % module,
                                                       namespace = app_name,
                                                       app_name = app_name))]

# Pour une raison inconnue, ces valeurs par défaut ne sont pas définies ...
handler404='django.views.defaults.page_not_found'
handler403='django.views.defaults.permission_denied'
handler500='django.views.defaults.server_error'
