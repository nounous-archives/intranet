import settings
from lc_ldap.variables import ldap_test_password

def _default_test(user):
    return user.has_perm('auth.crans_adherent')

def app_list(request):
    apps = []
    if request.user.is_authenticated():
        for app in settings.INTRANET_APPS + settings.REDIRECT_SERVICES:
            if app.get('test', _default_test)(request.user):
                apps.append(app)
            if 'category_label' not in app:
                app['category_label'] = settings.CATEGORY_LABELS.get(app['category'], app['category'])
        apps.sort(key = lambda x: (x['category'], 'label' in x and x['label'] or x['name'] ))
    return {'apps': apps}

def password_hint(request):
    """Phrase explicative qui donne le mot de passe potentiel, en env de test"""
    return {'password_hint': u'(hint: %s)' % ldap_test_password}

def global_message(request):
    return {'intranet_message' : settings.GLOBAL_MESSAGES}
