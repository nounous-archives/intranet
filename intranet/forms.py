# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.debug import sensitive_variables

from lc_ldap.shortcuts import lc_ldap
import ldap

class PassForm(forms.Form):
    """Formulaire de demande de mot de passe, avec vérification dans la base
    ldap. S'instancie avec un login crans"""
    passwd = forms.CharField(
                label=_(u'Mot de passe'),
                max_length=255,
                widget=forms.widgets.PasswordInput,
                required=True
            )

    def __init__(self, username, *args, **kwargs):
        """Initialise le formulaire, en prenant en entrée le login Crans
        dans `username`"""
        self.username = username
        super(PassForm, self).__init__(*args, **kwargs)

    @sensitive_variables('self.cleaned_data')
    def clean_passwd(self):
        try:
            conn = lc_ldap(user=self.username, cred=self.cleaned_data.get('passwd').encode('utf-8'))
        except ldap.INVALID_CREDENTIALS:
            raise forms.ValidationError(_(u"Mot de passe invalide"))

        return self.cleaned_data

