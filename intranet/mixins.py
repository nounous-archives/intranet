# -*- coding: utf-8 -*-
# Mixins de vérification de droits spécifiques

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from intranet import conn_pool
from intranet import settings

class CableurOrSelfMixin(object):
    """
      Mixin permettant de vérifier soit que l'utilisateur est câbleur soit
      qu'il consulte une page qui le concerne
    """
    @method_decorator(login_required)
    def dispatch(self, request, uid, *args, **kwargs):
        luser = conn_pool.get_user(request.user)
        if luser.get("aid", [""])[0] == uid or luser.get("cid", [""])[0] == uid or request.user.has_perm('auth.crans_cableur'):
            return super(CableurOrSelfMixin, self).dispatch(request, uid, *args, **kwargs)
        else:
            messages.error(request, _(u"Accès interdits, droits insuffisants"))
            return redirect(settings.LOGIN_URL)

class CableurMixin(object):
    """
      Mixin permettant de vérifier que l'utilisateur est câbleur
    """
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('auth.crans_cableur'):
            return super(CableurMixin, self).dispatch(request, *args, **kwargs)
        else:
            messages.error(request, _(u"Accès interdits, droits insuffisants"))
            return redirect(settings.LOGIN_URL)
