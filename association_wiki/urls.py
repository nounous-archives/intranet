from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.Index.as_view(), name='index'),
    url('^link$', views.LinkView.as_view(), name='link'),
    url('^forget$', views.Forget.as_view(), name='forget'),
    url('^create$', views.Create.as_view(), name='create'),
]
