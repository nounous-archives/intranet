# Create your views here.

import django.shortcuts
from django.contrib.auth.decorators import login_required


@login_required
def bonjour(request):
    return django.shortcuts.render(request, "dummy/bonjour.html", {"session" : request.session })
