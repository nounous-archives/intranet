# Crans Intranet2

L'intranet du Crans regroupe en un site web les services de l'association,
sois sous la forme d'applications, soit sous la forme de pointeurs vers
les services. L'intranet actuel (version 2) est basé sur django.

Actuellement, le code est compatible python 2.7 et django 1.10 (stretch). Il
utilise une base `pgsql` pour son fonctionnement interne, mais la plupart
des applications se base sur les données `ldap` et nécessitent `lc_ldap`.

## Dépendances Debian
 * python-django
 * python-django-reversion
 * python-netaddr
 * python-unidecode
 * python-passlib
 * python-beautifulsoup
 * python-requests
 * python-ipaddr
 * python-ldap
 * texlive
 * texlive-latex-extra
 * texlive-lang-french
 * snimpy
 * python-snimpy
 * snmp-mibs-downloader
 * libfile-mimeinfo-perl

## Dépendances Crans
L'intranet est donc intrinsèquement lié à `/usr/scripts`. Celui-ci est
d'ailleurs mentionné dans le `sys.path`.

 * `/usr/scripts` (https://gitlab.crans.org/nounous/scripts.git)
 * `/usr/scripts/lc_ldap` (https://gitlab.crans.org/nounous/lc_ldap)
 * `/usr/scripts/wifi_new` (pour la wifimap) (https://gitlab.crans.org/nounous/wifimap)

En cas d'utilisation du CAS crans pour le login, le module `django_cas_ng`
doit être installé via `pip`.

## Après chaque pull
Après avoir configuré le dépôt et à chaque pull, il est utile de lancer
la commande `./update.sh`. Celle-ci réalise (entre-autre) les opérations
suivantes :
 * Efface les `*.pyc` résiduels
 * Met à jour les fichiers compilés de traduction de chaque application
 * Applique les nouvelles migrations
 * Collecte les fichiers statiques (pour le serveur web) dans `./var/static`

## Vieille installation de test
Si vous installez une copie toute propre, vous pouvez sauter cette partie.
Si vous venez de mettre à jour, il vous faut :
 * Effacer `settings_local.py`
 * Reconfigurer une copie de test à l'aide de la nouvelle méthode (ci-dessous)

## Installation dans un virtualenv
L'intranet ne prod n'est actuellement pas installé dans un virtualenv. Néanmoins, une installation
dans un virtualenv a l'avantage de pouvoir contrôler finement les versions des lib python utilisé
et de se mettre très facilement en place.

1. Pour construire un virtualenv pour l'intranet vous aurez besoin d'installet les paquets debian
   suivant :

        sudo apt-get install virtualenv pip libsmi2-dev libsasl2-dev postgresql-server-dev-all \
        libsnmp-dev libzmq3-dev libczmq-dev

2. Installer les Dépendances Crans (vois section du même nom)

3. Clonez le dépot de l'intranet::

        $ git clone https://gitlab.crans.org/nounous/intranet.git
        $ cd intranet

4. Y initialisé un virtualenv et l'activer::

        $ virtualenv ./
        $ . bin/activate

   lancer simplement `deactivate` pour le désactiver par la suite.
   Activer un virtualenv ne fait grosso modo qu'une seule chose: mettre le dossier `bin/` du
   virtualenv en premier dans votre PATH.

   Ici, dans le virtualenv, les libs pythons du système ne sont pas accessible. En conséquence,
   lors de l'installation des dépendances au point ci-dessous, les libs seront réinstallé dans le
   virtualenv même si une lib system satifait la dépendance.
   Pour pouvoir utiliser les libs systèmes, il faut créer le virtualenv comme suit :

        $ virtualenv --system-site-packages ./

   Personnellement (Nit) en dev je préfère ne pas utiliser l'option `--system-site-packages` comme
   ça je suis sûr que mon système n'interfère pas et que seulement les lib installé dans le
   virtualenv sont utilisé par l'application sur laquelle je travaille. A contrario, en prod, je
   préfère utiliser l'option `--system-site-packages` et utiliser le plus possible de lib du
   système. Comme ça, c'est mon système qui se charge de mettre a jour ces lib et je n'ai pas a
   penser a me rendre dans chaque virtualenv pour les mettre a jour.

   Vous pouvez lancer `pip freeze -l` pour lister les libs installées dans le virtualenv.

5. Installer les dépendances nécessaire avec pip::

        $ pip install -r requirements.txt

6. Suivre la section Configuration d'une copie de test ci dessous.


## Configuration d'une copie de test

Pour faire marcher l'intranet :

 * Créer un fichier `CRANS_SCRIPTS_ROOT` à la racine (`touch CRANS_SCRIPTS_ROOT`)
 * Copier un fichier `testing.sh` depuis `/usr/scripts` et
   * rajouter une ligne `export CPATH=/usr/scripts` pour indiquer où se trouve
     le dépôt `usr-scripts` du Crans
   * Mettre à jour `DBG_INTRANET` pour l'adresse de l'intranet (par exemple
        `"http://vo.crans.org:8080"`)
   * Modifier le reste des variables à votre goût (voir ci-dessous)

 * exécuter `./update.sh`
 * Pour le lancer : `./manage.py runserver $host:$port`
    Ex : `./manage.py runserver vo.crans.org:8080`

Une copie du dépôt, pré-installé, est présente sur vo (cf "prod sur vo" ci-dessous),
avec une configuration très proche de celle de la prod afin de pouvoir réaliser
des tests réalistes. Des bases de données locales sont donc présentes, et vous
pouvez les utiliser, que ce soit depuis votre dépôt, ou celui pré-installé
Regardez les variables `DBG_DJANGO_DB`, `DBG_LDAP` etc.
Il est même possible de les utiliser sur un dépôt distant, sur votre pc perso
(lire la partie tunelling de `testing.sh`, et penser
à installer toutes les dépendances chez vous !)


## Fonctionnement en prod

Le serveur habituel de production est `o2.crans.org`, qui sert les pages webs
via `nginx`, en rajoutant le https.

 * `nginx` sert directement les fichiers statiques, dont l'uri commence par
   `/static/` depuis le répertoire local `var/static/`. Ce repertoire est
   peuplé via la commande `umask 002; ./manage.py collectstatic`.
 * Les autres requêtes https sont traités en mode proxy, c'est-à-dire qu'elles
   sont redirigés vers une socket locale `/tmp/gunicorn-intranet.sock` servie
   par `gunicorn`.

La base de donnée pgsql n'est _pas_ sur `o2` : une connexion distante doit
être établie avec les paramètres suivants :

 * Serveur : `pgsql.adm.crans.org` (actuellement `thot`)
 * Nom de base : `django`
 * Utilisateur : `crans`

L'authentification se fait à l'aide d'`ident` ce qui permet de s'affranchir du
mot de passe. Certains services ont également besoin d'accéder aux données
des modèles django, et lorsque cela est possible, on leur autorise un accès
en lecture seule à l'aide de l'utilisateur `crans_ro`.

## Fonctionnement en "prod sur vo"

Un fonctionnement très proche de `o2` est mis en place sur `vo`. Le dépôt se
trouve aussi dans `/usr/local/django`, mais les bases de données sont toutes
locales à `vo`. À noter la présence d'un fichier `testing.env` utilisé par
l'unit file.
Vous pouvez générer ce fichier à partir du `testing.sh` via la commande :
`. testing.sh && (echo "PYTHONPATH=/usr/local/scripts" && env | grep "^DBG_") > testing.env`

Le site est accessible via https://intranet-dev.crans.org/

## Cloner la base pgsql de prod sur vo

Sur `thot`, lancer (dans votre home) :
 * `sudo -u postgres pg_dump django -C -f /tmp/django_save`
 * `sudo mv /tmp/django_save .`

Sur `vo`, lancer :
 * `sudo -u postgres psql -c "DROP DATABASE IF EXISTS django"`
 * `sudo -u postgres psql -f django_save`
 * `rm django_save`

Il reste deux trois modifs à faire, notamment, régler le site correctement
sur l'interface d'admin ( https://intranet-dev.crans.org/admin )

## Développer pour l'intranet
Si vous souhaitez contribuer à l'intranet veuillez lire le [guide de développeur](doc/dev_guide.md)

## Todo-List
 * drop column type (reversion_version)
 * fin de l'appli d'impression
 * comment dumper la base de prod ici
 * crans/crans_ro information
 * env de test plus propre
