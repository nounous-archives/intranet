# -*- coding: utf-8 -*-
# Application de cablage sur l'intranet
# Gabriel Détraz detraz@crans.org


from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from intranet import conn_pool
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden

from intranet.settings import BASE_LDAP_TEST

import lc_ldap.shortcuts

from forms import MachineForm, AdherentForm, ClubForm, FactureForm


if BASE_LDAP_TEST:
    use_ldap_admin = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
        constructor=lc_ldap.shortcuts.lc_ldap_test)
else:
    use_ldap_admin = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
        constructor=lc_ldap.shortcuts.lc_ldap_admin)

LDAP_FILTER = {
u'uid' : u'uid',
u'chambre' : u'chbre',
u'nom' : u'nom',
u'telephone' : u'tel',
u'prenom' : u'prenom',
u'mail' : u'mail',
u'fid' : u'fid',
u'uid_club' : u'uid',
u'local' : u'chbre',
u'nom_club' : u'nom',
u'cid' : u'cid',
u'host' : u'host',
u'mac_address' : u'macAddress',
u'ip_host_number' : u'ipHostNumber',
u'mid' : u'mid',
u'aid' : u'aid',
}

@login_required
def afficher(request):
    """ Accueil de l'interface de cablage  """
    if not request.user.has_perm('auth.crans_cableur'):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")
    return render(request, "cablage/affichage.html")

@use_ldap_admin
@login_required
def rechercher(request, ldap, objet):
    """ Interface de recherche d'un objet ldap  """
    if not request.user.has_perm('auth.crans_cableur'):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")
    if request.method == "POST":
        machineform = MachineForm(request.POST)
        adherentform = AdherentForm(request.POST)
        clubform = ClubForm(request.POST)
        factureform = FactureForm(request.POST)
        formchanged = []
        formcleaned = dict()
        if machineform.is_valid():
            formchanged += machineform.changed_data
            formcleaned.update(machineform.cleaned_data)
        if clubform.is_valid():
            formchanged += clubform.changed_data
            formcleaned.update(clubform.cleaned_data)
        if adherentform.is_valid():
            formchanged += adherentform.changed_data
            formcleaned.update(adherentform.cleaned_data)
        if factureform.is_valid():
            formchanged += factureform.changed_data
            formcleaned.update(factureform.cleaned_data)
        if machineform.is_valid() or clubform.is_valid() or adherentform.is_valid() or factureform.is_valid():
            filtre = u""
            result = None
            for champ in formchanged:
                valeur = formcleaned[champ]
                if valeur != "*":
                    filtre += u"(%s=%s)" % (LDAP_FILTER[champ],valeur)
            if filtre != u"":
                donnees = ldap.search(u'(&%s)' % filtre)
                # Tri des objets suivant ce qu'on veut à l'arrivée
                results = []
                for item in donnees:
                    if objet in item.ldap_name or item.ldap_name in objet:
                        results.append(item)
                    else:
                        # Si on cherche un adh à partir de ses machines ou factures
                        # Note : objet peut etre addmachineadh , donc on ratisse large
                        if 'adherent' in objet or 'club' in objet:
                            if "machine" in item.ldap_name  or item.ldap_name == "facture":
                                if item.proprio().ldap_name == objet:
                                    results.append(item.proprio())
                        elif objet == 'facture':
                            if item.ldap_name == "adherent" or item.ldap_name == "club":
                                results += item.factures()
                        elif objet == 'machine':
                            if item.ldap_name == "adherent" or item.ldap_name == "club":
                                results += item.machines()
                return render(request, "cablage/recherche.html", {
                    'adherentform': adherentform,
                    'machineform': machineform,
                    'factureform': factureform,
                    'clubform': clubform,
                    'objet': objet,
                    'results': results,
                })
            return render(request, "cablage/recherche.html", {
                'adherentform': adherentform,
                'machineform': machineform,
                'factureform': factureform,
                'clubform': clubform,
                'objet': objet,
            })
        else:
            return render(request, "cablage/recherche.html", {
                'adherentform': adherentform,
                'machineform': machineform,
                'factureform': factureform,
                'clubform': clubform,
                'objet': objet,
            })
    else:
        machineform = MachineForm()
        adherentform = AdherentForm()
        clubform = ClubForm()
        factureform = FactureForm()
        context = {'machineform': machineform, 'adherentform': adherentform, 'clubform': clubform, 'factureform': factureform, 'objet': objet}
        return render(request, "cablage/recherche.html", context)
