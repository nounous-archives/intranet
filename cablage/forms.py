# -*- coding: utf-8 -*-
from django import forms

from django.utils.translation import ugettext_lazy as _

class BaseForm(forms.Form):
    """ Formulaire de recherche pour le cableur """
    uid = forms.CharField(label=u'Login', max_length=40, required=False)
    chambre = forms.CharField(label=_(u'Chambre'), max_length=5, required=False)
    nom = forms.CharField(label=_(u'Nom'), max_length=40, required=False)

class AdherentForm(BaseForm):
    telephone = forms.CharField(label=_(u'Téléphone'), max_length=10, required=False)
    aid = forms.IntegerField(label=u'Aid', required=False)
    prenom = forms.CharField(label=_(u'Prénom'), max_length=40, required=False)
    mail = forms.EmailField(label=u'Mail', required=False)


class FactureForm(forms.Form):
    fid = forms.IntegerField(label=u'Fid', required=False)

class ClubForm(forms.Form):
    uid_club = forms.CharField(label=u'Login', max_length=40, required=False)
    local = forms.CharField(label=_(u'Local'), max_length=5, required=False)
    nom_club = forms.CharField(label=_(u'Nom'), max_length=40, required=False)
    cid = forms.IntegerField(label=u'Cid', required=False)

class MachineForm(forms.Form):
    host = forms.CharField(label=_(u'Nom de machine'), max_length=40, required=False)
    mac_address = forms.CharField(label=u'Mac', max_length=17, required=False)
    ip_host_number = forms.CharField(label=u'IP', max_length=16, required=False)
    mid = forms.IntegerField(label=u'Mid', required=False)



