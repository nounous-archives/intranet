from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.afficher, name='afficher'),
    url('rechercher/(?P<objet>[^/]*)?/?$', views.rechercher, name="rechercher"),
]
