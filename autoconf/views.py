# Create your views here.

import django.shortcuts
from django.contrib.auth.decorators import login_required

from django.template import RequestContext
from intranet import conn_pool

def mozilla(request):
    emailaddress = request.GET.get("emailaddress", '%EMAILLOCALPART%')
    username = emailaddress.split(u'@', 1)[0]
    conn = conn_pool.get_conn(request.user)
    ret = conn.search(u"(|(mail=%(p)s)(canonicalAlias=%(p)s))" % {'p': emailaddress})
    if ret:
        username = unicode(ret[0]['uid'][0])
    return django.shortcuts.render(request,
        "autoconf/mozilla.xml", {
            "session": request.session,
            "username": username,
        },
        content_type="text/xml; charset=utf-8")
