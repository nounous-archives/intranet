# -*- coding: utf-8 -*

import os
from subprocess import call
from tempfile import mkdtemp, mkstemp
from django.template.loader import render_to_string

from django.shortcuts import render,  redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.template import RequestContext

from django.http import HttpResponse

from tex import render_tex
import models
import hashlib
import time
from intranet import settings, conn_pool

from django.views.generic import View

from intranet.mixins import CableurOrSelfMixin, CableurMixin

class IndexView(CableurOrSelfMixin, View):
    """
      Classe de base pour l'index
    """
    template_name = "factures/index.html"

    def get(self, request, aid, cid, ptype, *args, **kwargs):
        if ptype=="adh":
            luser = conn_pool.get_conn(request.user).search(u'aid=%s' % aid)[0]
        elif ptype=="club":
            luser = conn_pool.get_conn(request.user).search(u'cid=%s' % cid)[0]
        return render(
            request,
            self.template_name,
            {'luser': luser,},
        )

    @method_decorator(login_required)
    def dispatch(self, request, aid=None, cid=None, ptype=None, *args, **kwargs):
        if not aid and not cid:     # Si aid=None, on renvoit celui de l'user
            luser = conn_pool.get_user(request.user)
            if luser.ldap_name == 'club':
                cid = luser['cid'][0]
                ptype = 'club'
            else:
                aid = luser['aid'][0]
                ptype = 'adh'
        return super(IndexView, self).dispatch(request, aid, cid, ptype, *args, **kwargs)

index = IndexView.as_view()

class PDFView(View):
    """
        Classe de view servant de base à la génération de PDF pour les
        adhérents.
    """
    tex_template = '404.tex'
    context = {}

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PDFView, self).dispatch(*args, **kwargs)

    def get(self, request):
        return render_tex(
            request,
            self.tex_template,
            self.context,
        )

class FactureView(PDFView):
    """
        View générant le fichier PDF d'une facture.
    """
    tex_template = 'factures/facture.tex'

    def get(self, request, fid):
        luser = conn_pool.get_user(request.user)
        f = conn_pool.get_conn(request.user).search(u"fid=%s" % int(fid))
        if not f: # Si la facture n'existe pas
            return redirect('factures:index')
        else: # Sinon, le premier élément de f est la facture.
            f = f[0]

        # On vérifie que l'utilisateur demande une facture dont il est le
        # propriétaire, ou que l'utilisateur est un câbleur.
        if not f.dn in [fac.dn for fac in luser.factures()] \
            and not request.user.has_perm('auth.crans_cableur'):
            return redirect('factures:index')

        # On commence le traitement des données à générer.
        total = 0
        for i in range(0,len(f['article'])):
            f['article'][i].value['ptotal'] = int(f['article'][i]['nombre']) * float(f['article'][i]['pu'])
            tmp = f['article'][i].value['code']
            f['article'][i].value['code'] = tmp.replace("_", "\_")
            del tmp
            total += f['article'][i].value['ptotal']
        if f.get('recuPaiement', []):
            paid = total
        else:
            paid = 0

        # On récupère la date et l'heure de la facture
        (jour, heure) = f['historique'][0].value.split(',')[0].split(" ")
        date = u"Le %s à %s" % (jour, heure.replace(":", "h"))
        # On récupère le montant restant à payer
        topay = total - paid

        self.context = {
            'total':total,
            'paid':paid,
            'topay':topay,
            'DATE':date,
            'f':f,
            'fid':fid,
        }

        return super(FactureView, self).get(request)

facture = FactureView.as_view()

class FormulaireView(PDFView):
    """
        View générant le fichier PDF d'un formulaire d'adhésion ou de
        réadhésion.
    """
    tex_template = 'factures/formulaire-adhesion.tex' # Dans usr/scripts ?

    def get(self, request, fid):
        luser = conn_pool.get_user(request.user)
        f = conn_pool.get_conn(request.user).search(u"fid=%s" % int(fid))
        if not f: # Si la facture n'existe pas
            return redirect('factures:index')
        else: # Sinon, le premier élément de f est la facture.
            f = f[0]

        # On vérifie que l'utilisateur demande une facture dont il est le
        # propriétaire, ou que l'utilisateur est un câbleur.
        if not f.dn in [fac.dn for fac in luser.factures()] \
            and not request.user.has_perm('auth.crans_cableur'):
            return redirect('factures:index')

        total = 0
        for i in range(0,len(f['article'])):
            f['article'][i].value['ptotal'] = int(f['article'][i]['nombre']) * float(f['article'][i]['pu'])
            total += f['article'][i].value['ptotal']
        if f.get('recuPaiement', []):
            paid = total
        else:
            paid = 0

        # On récupère la date et l'heure de la facture
        (jour, heure) = f['historique'][0].value.split(',')[0].split(" ")
        date = u"Le %s à %s" % (jour, heure.replace(":", "h"))

        self.context = {
            'paid': paid,
            'DATE': date,
            'f': f,
            'fid': fid,
        }

        return super(FormulaireView, self).get(request)

formulaire = FormulaireView.as_view()
