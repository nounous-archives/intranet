from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.index, name='index'),
    url('^liste/(?P<ptype>club)/(?P<cid>[0-9]+)/$', views.index, name='liste'),
    url('^liste/(?P<ptype>adh)/(?P<aid>[0-9]+)/$', views.index, name='liste'),
    url('^(?P<fid>[0-9]+).pdf', views.facture, name='facture'),
    url('^liste/(?P<ptype>adh)/(?P<aid>[0-9]+)/(?P<fid>[0-9]+).pdf/$',
        views.facture,
        name='facture'
    ),
    url('^liste/(?P<ptype>club)/(?P<cid>[0-9]+)/(?P<fid>[0-9]+).pdf/$',
        views.facture,
        name='facture'
    ),
    url('formulaire_(?P<fid>[0-9]+).pdf',
        views.formulaire,
        name='formulaire'
    ),
    url('^liste/(?P<ptype>adh)/(?P<aid>[0-9]+)/formulaire_(?P<fid>[0-9]+).pdf/$',
        views.formulaire,
        name='formulaire'
    ),
    url('^liste/(?P<ptype>club)/(?P<cid>[0-9]+)/formulaire_(?P<fid>[0-9]+).pdf/$',
        views.formulaire,
        name='formulaire',
    ),
]
