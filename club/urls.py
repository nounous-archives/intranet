from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.IndexView.as_view(), name='index'),
    url('^liste/(?P<cid>[0-9]+)/$', views.IndexView.as_view(), name='index'),
    url('^(?P<cid>[0-9]+)/imprimeur/ajouter$', views.ImprimeurAddView.as_view(), name='imprimeur_add'),
    url('^(?P<cid>[0-9]+)/imprimeur/retirer$', views.ImprimeurDelView.as_view(), name='imprimeur_del'),
    url('^(?P<cid>[0-9]+)/responsable/edit$', views.ResponsableEditView.as_view(), name='responsable_edit'),
    url('^(?P<cid>[0-9]+)/imprimeur/ajouter/(?P<cablage>cablage)$', views.ImprimeurAddView.as_view(), name='imprimeur_add'),
    url('^(?P<cid>[0-9]+)/imprimeur/retirer/(?P<cablage>cablage)$', views.ImprimeurDelView.as_view(), name='imprimeur_del'),
    url('^(?P<cid>[0-9]+)/responsable/edit/(?P<cablage>cablage)$', views.ResponsableEditView.as_view(), name='responsable_edit'),
    ]
