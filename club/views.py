# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.forms.models import formset_factory
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from forms import ImprimeurAddForm, ImprimeurDelForm, ResponsableEditForm
from tools import get_clubs_for_user, get_imprimeurs_for_club

from intranet import conn_pool

class IndexView(TemplateView):
    template_name = 'club/index.html'

    def get_context_data(self, cid = None, *args, **kwargs):
        context = super(IndexView, self).get_context_data(*args, **kwargs)
        clubs = []
        if cid and self.request.user.has_perm('auth.crans_cableur'):
            club = conn_pool.get_conn(self.request.user).search(u'cid=%s' % cid)[0]
            user_clubs = [(unicode(club['nom'][0]),club)]
            context.update({'cablage' : True})
        else:
            user_clubs = get_clubs_for_user(self.request.user)
        for (club_name, club) in user_clubs:
            imprimeurs = get_imprimeurs_for_club(club)
            club_dict = {
                'club_name': club_name,
                'cid': int(club['cid'][0]),
                }
            if imprimeurs:
                club_dict.update({
                    'imprimeurs': zip(*get_imprimeurs_for_club(club))[0],
                })
            else:
                club_dict.update({
                    'imprimeurs': [],
                })
            responsable = club['responsable'][0].get_respo()
            club_dict.update({
                'responsable': unicode(responsable['prenom'][0]) +u' ' + unicode(responsable['nom'][0])
            })
            clubs.append(club_dict)
        context.update({'clubs': clubs})
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)


class EditClubView(FormView):
    success_url = reverse_lazy('club:index')

    def get_context_data(self, *args, **kwargs):
        context = super(EditClubView, self).get_context_data(*args, **kwargs)
        if 'cablage' in self.kwargs:
            context.update({'cablage':True,'cid':self.cid})
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        self.cid = int(self.kwargs['cid'])
        if 'cablage' in self.kwargs:
            self.success_url = reverse_lazy('club:index', args=[(self.cid)])
        if not self.request.user.has_perm('auth.crans_cableur'):
            for club_name, club in get_clubs_for_user(self.request.user):
                if int(club['cid'][0]) == self.cid:
                    break
            else: # Ce else est pour le for et non pour le if
                messages.error(self.request, _("Vous n'avez pas le droit de modifier ce club !"))
                return redirect('club:index')
        return super(EditClubView, self).dispatch(*args, **kwargs)

class ImprimeurAddView(EditClubView):
    template_name = 'club/imprimeur_add.html'
    form_class = ImprimeurAddForm

    def form_valid(self, form):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        with club as club:
            new_imprimeur = form.cleaned_data['new_imprimeur']
            try:
                new_imprimeur_ldap = ldap_user.search(u'uid=%s' % new_imprimeur)[0]
            except IndexError:
                messages.error(self.request, _("Ce login n'est pas valide"))
                return redirect(self.request.path)
            try:
                club['imprimeurClub'].append(unicode(new_imprimeur_ldap['aid'][0]))
                club.history_gen()
                club.save()
            except ValueError as e:
                messages.error(self.request, e)
                return redirect(self.request.path)
        return super(ImprimeurAddView, self).form_valid(form)

class ImprimeurDelView(EditClubView):
    template_name = 'club/imprimeur_del.html'
    form_class = ImprimeurDelForm

    def get_form(self, form_class):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        post = self.request.POST
        if post:
            return form_class(club, post)
        else:
            return form_class(club)

    def form_invalid(self, form):
        print self.request.POST
        print form.non_field_errors()
        print form.errors
        print form.__dict__
        return super(ImprimeurDelView, self).form_invalid(form)

    def form_valid(self, form):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        with club as club:
            for old_imprimeur_aid in form.cleaned_data['old_imprimeurs']:
                club['imprimeurClub'].remove(unicode(old_imprimeur_aid))
                club.history_gen()
                club.save()
        return super(ImprimeurDelView, self).form_valid(form)

class ResponsableEditView(EditClubView):
    template_name = 'club/responsable_edit.html'
    form_class = ResponsableEditForm

    def form_valid(self, form):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        with club as club:
            new_responsable = form.cleaned_data['new_responsable']
            try:
                new_responsable_ldap = ldap_user.search(u'uid=%s' % new_responsable)[0]
            except IndexError:
                messages.error(self.request, _("Ce login n'est pas valide"))
                return redirect(self.request.path)
            try:
                club['responsable'] = unicode(new_responsable_ldap['aid'][0])
                club.history_gen()
                club.save()
            except ValueError as e:
                messages.error(self.request, e)
                return redirect(self.request.path)
        return super(ResponsableEditView, self).form_valid(form)

