from django.contrib import admin
from models import Jobs

@admin.register(Jobs)
class JobsAdmin(admin.ModelAdmin):
    list_display = ('jobname', 'login', 'starttime', 'endtime', 'result', 'club', 'jid')
    list_filter = ('login', 'result')

