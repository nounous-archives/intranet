# -*- encoding: utf-8 -*-

# Vincent Legallic, Gabriel Détraz, Charlie Jacomme

from intranet import settings, conn_pool

import time
import django.shortcuts
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.shortcuts import render_to_response,render
from django.views.generic.detail import DetailView
from django.utils.translation import ugettext_lazy as _
from django.forms import formset_factory

# Paginateur , afin d'éviter de trop longues pages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Pour stocker des messages dans la session courante
from django.contrib import messages

# On importe l'utilitaire d'impression
from impression.base import FichierInvalide
from impression.impression_hp import impression

# Formulaires
from forms import UploadForm, PrintForm, ClubForm, UploadFormSet
from models import Jobs
from digicode.models import Code
from printer.models import Printer

# config_impression, pour les prix
import gestion.config.impression as config_impression

# Shortcuts pour recrediter, et la regexp des jobs effectués
from lc_ldap import shortcuts

# Quand on arrive pas à contacter l'imprimante
from urllib2 import URLError
# L'URL pour tester qu'on peut bien envoyer des impressions
import requests
from impression.impression_hp import URL_STATE, CA

config_prix = {"c_face_nb": config_impression.c_face_nb,
            "c_face_couleur": config_impression.c_face_couleur,
            "c_agrafe": config_impression.c_agrafe,
            "amm": config_impression.amm,
            "c_a4": config_impression.c_a4,
            "c_a3": config_impression.c_a3}

# Pour créer les dossier et faire des pdfinfo
import subprocess
import re
import os, os.path


class MainPrinterView(DetailView):
    model = Printer
    template_name = 'impressions/imprimante.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(DetailView, self).dispatch(*args, **kwargs)


def get_login(request):
    """Renvoie le login de l'utilisateur connecté"""
    # Attention, un intranet dirty hack fait que les users LDAP sont en login@crans.org
    login = request.user.username
    login = login.split("@")[0]
    return login

def is_imprimeur(request):
    """Renvoie True si l'utilisateur connecté à les droits imprimeurs"""
    return request.user.has_perm('auth.crans_imprimeur')

def get_solde(request):
    luser = conn_pool.get_user(request.user)
    try:
        solde = luser['solde'][0]
    except IndexError:
        solde = 0
    return solde

def get_solde_clubs(request):
    luser = conn_pool.get_user(request.user)
    class solde_clubs:
        def __init__(self):
            self.solde = 0
    clubs = []
    for cl in luser.clubs() + luser.imprimeur_clubs():
        try:
            club = solde_clubs()
            club.nom = cl['nom'][0]
            club.uid = cl['uid'][0]
            club.solde = cl['solde'][0]
        except KeyError:
            pass
        except IndexError:
            pass
        clubs.append(club)

    return clubs


def get_club(request):
    luser = conn_pool.get_user(request.user)
    club = []
    for cl in luser.clubs() + luser.imprimeur_clubs():
        try:
            nom = cl['nom'][0]
            uid = cl['uid'][0]
            if (uid,nom) not in club:
                club.append((uid,nom))
        except KeyError:
            pass
    club = tuple(club)
    return club

from forms import get_storage_path

@login_required
def view(request):
    ldap_user = conn_pool.get_user(request.user)
    if request.method == "GET":
        uploadform = formset_factory(UploadForm, formset=UploadFormSet)
        clubform = ClubForm(ldap_user)
        return render(request, "impressions/impressions.html", {
            'uploadform': uploadform,
            'clubform' : clubform,
            'printers': Printer.objects.all(),
        })
    else:
        if "SubmitUploadForm" in request.POST.keys():
            # On formate et on instancie pour les clubs
            clubform = ClubForm(ldap_user, request.POST)
            # Si impression en tant que club, on récupère le resultat
            club = False
            if clubform.is_valid() and not clubform.cleaned_data['club_list'] == ldap_user['uid'][0]:
                club = clubform.cleaned_data['club_list']

            login = get_login(request)
            # Si on imprime en tant que club, on utilise le login club
            if club:
                login_used = club
                clubsolde = {i.uid : i.solde for i in get_solde_clubs(request)}
                solde = clubsolde[login_used]
            else:
                login_used = login
                solde = get_solde(request)

            uploadform = formset_factory(UploadForm, formset=UploadFormSet)
            uploadform = uploadform(request.POST, request.FILES)

            # On passe les datas nécessaires à l'initialisation aux forms.
            for form in uploadform:
                form.real_user = login_used
                form.login = login
                form.club = club

            if uploadform.is_valid():
                initial_data = [{'filenom': form.cleaned_data["fichier"].name,
                                 'pageno' : form.cleaned_data["pageno"],} for form in uploadform if form.cleaned_data.has_key("fichier")]
                printform = formset_factory(PrintForm, extra=0)
                printform = printform(initial=initial_data)
                return render(request, "impressions/print.html",
                    {'printform': printform,
                     'une_agrafe': ["hg", "hd", "bg", "bd"],
                     'deux_agrafes': ["g", "d"],
                     'solde' : get_solde(request),
                     'solde_club' : get_solde_clubs(request),
                     'solde_actif': solde,
                     'config_prix': config_prix,
                     'uploadform' : uploadform,})
            else:
                for form_errors in uploadform.errors:
                    for errorlist in form_errors.values():
                        for error in errorlist:
                            messages.error(request, error)
                for error in uploadform.non_form_errors():
                    messages.error(request, error)
                uploadform = formset_factory(UploadForm, formset=UploadFormSet)
                clubform = ClubForm(ldap_user)
                return render(request, "impressions/impressions.html",\
                    {'uploadform': uploadform,
                     'clubform' : clubform})
        elif "SubmitPrintForm" in request.POST.keys():
        # Avant toute chose, on vérifie l'état de l'imprimante, sinon on redirige
            try:
                req = requests.get(URL_STATE, verify=CA)
            except requests.exceptions.ConnectionError:
                return django.shortcuts.render_to_response("impressions/affichage.html")

            login = get_login(request)
            printform = formset_factory(PrintForm)
            printform = printform(request.POST)
            if printform.is_valid():
                jobs, jos = [], []
                prix = 0
                for form in printform:
                    copies = form.cleaned_data['copies']

                # On formate les options pour la couleur
                    cl = form.cleaned_data['couleur']
                    if cl=="couleur":
                        couleur = True
                    else:
                        couleur = False

                    format = form.cleaned_data['format']

                # On formate la disposition
                    disp = form.cleaned_data['disposition']
                    if disp=="recto-verso":
                        rv = True
                    else:
                        rv = False
                    if disp=="livret":
                        livr = True
                    else:
                        livr = False

                # On formate les options pour les agrafes
                    agra = form.cleaned_data['agrafes']
                    if agra=="hg":
                        agrafe="TopLeft"
                    elif agra=="hd":
                        agrafe="TopRight"
                    elif agra=="g":
                        agrafe="Left"
                    elif agra=="d":
                        agrafe="Right"
                    else:
                        agrafe="None"

                    perforation = form.cleaned_data['perforation']

                    # On récupère le nom du fichier auprès de la form qui va bien
                    filename = form.cleaned_data['filenom']
                    # On prend dans la db, le plus recent, qui correspond au nom de fichier et à l'user
                    jos.append(Jobs.objects.filter(file=filename,login=login).latest('id'))
                    if jos[-1].club:
                        login_used = jos[-1].club
                        clubsolde = {i.uid : i.solde for i in get_solde_clubs(request)}
                        solde = float(clubsolde[login_used])
                    else:
                        login_used = login
                        solde = get_solde(request)

                    try:
                        jobs.append(impression(get_storage_path(login_used, filename), adh=login_used))
                    except FichierInvalide:
                        messages.error(
                            request,
                            _(u"Le fichier %(filename)s ne semble pas être un PDF") % {'filename' : filename}
                        )
                        return django.shortcuts.redirect("/impressions/")
                # On met à jour les options et on envoie
                    jobs[-1].changeSettings(papier=format,
                                           couleur=couleur,
                                           copies=copies,
                                           agrafage=agrafe,
                                           perforation=perforation,
                                           livret=livr,
                                           recto_verso=rv)
                    prix += float(jobs[-1]._calcule_prix())

                try:
                # On vérifie que le compte est suffisamment approvisionné.
                    if solde < prix:
                        raise ValueError(u"Solde insuffisant pour effectuer l'impression")
                # On vérifie que le compte est suffisamment approvisionné.
                # Toutefois, on a pas confiance, et on est prêt à arrêter l'impression si le compte n'a pas assez.
                    for job, jo in zip(jobs,jos):
                        job.imprime()
                        jo.jid = job.get_jid()
                        jo.result = "Running"
                        jo.prix = float(job._calcule_prix())
                        jo.save()
                except ValueError:
                    messages.error(request, _(u"Solde insuffisant pour effectuer l'impression") )
                    return django.shortcuts.redirect("/impressions/")
                # On récupère le job id
                unused_digits=Code.gen_random_code()
                code = Code.objects.create(digits=unused_digits, owner=request.user)
                messages.success(request, _(u"""Votre fichier a bien été envoyé à l'impression \n Le code pour entrer dans le batiment J est B7806 \n Un code pour entrer dans le local impression (4 ème étage) a été généré : """) + unicode(unused_digits) + u"""#""")
                return django.shortcuts.redirect("/impressions/")
            else:
                 return HttpResponseBadRequest(_(u"Formulaire non pris en charge."))
        else:
            return HttpResponseBadRequest(_(u"Formulaire non pris en charge."))

@login_required
def gestion(request):
    login = get_login(request)
    imprimante_jobs = []
    lpq_jobs = []

    if not is_imprimeur(request):
        # On peuple avec les logins clubs de l'adh
        logins = [ unicode(cl[0]) for cl in get_club(request)]
        logins.append(login)
        ended_jobs_list = Jobs.objects.filter(login__in = logins).order_by('-jid')
    else:
        ended_jobs_list = Jobs.objects.all().order_by('-jid')

    paginator = Paginator(ended_jobs_list, 50)
    page = request.GET.get('page')
    try:
        ended_jobs = paginator.page(page)
    except PageNotAnInteger:
        ended_jobs = paginator.page(1)
    except EmptyPage:
        ended_jobs = paginator.page(paginator.num_pages)

    #On récupère les jobs en attente
    class current_jobs:
        def __init__(self):
            self.login="None"
            self.file="Unknown"

    lpq_proc = subprocess.Popen(["lpq"], stdout=subprocess.PIPE)
    lpq_request = lpq_proc.stdout.readlines()[2:]

    # Si il y a des jobs en attente, on peuple la file lpq_jobs
    if lpq_request:
        for job in lpq_request:
            job = job.split()
            current = current_jobs()
            current.jid = job[2]
            # Important : sur l'intranet, le login est dans la tache (owner : www-data)
            try:
                current.file = job[3].split(":")[2].decode('utf-8', errors="replace")
                current.login = job[3].split(":")[1]
            # Si c'est pas une tache intranet, on traite classiquement
            except IndexError:
                current.file = job[3].decode('utf-8', errors="replace")
                current.login = job[1]
            current.ordre = job[0]


            # On affiche si imprimeur ou proprio de la tache, ou respo des clubs concernés
            if current.login == login or current.login in [ unicode(cl[0]) for cl in get_club(request)] or is_imprimeur(request):
                lpq_jobs.append(current)

    # On précise si la personne est imprimeur
    imprimeur = is_imprimeur(request)

    return render(request, "impressions/liste.html",
                              {"ended_jobs" : ended_jobs,
                               "lpq_jobs" : lpq_jobs,
                               "imprimeur" : imprimeur,
                               "imprimante_jobs" : imprimante_jobs})


def make_recredit(request, crans_jid, imprimeur_not_required=False):
    # SI impression en tant que club, on change le login
    # On effectue le recredit également en récupérant le prix de la tache
    prix = False
    owner = False
    login = get_login(request)
    try:
        jo = Jobs.objects.get(jid=crans_jid)
        if jo.club:
            login = jo.club
            owner = jo.club
        else:
            owner = jo.login
        prix = jo.prix
        crans_jid = jo.jid
        task = jo.file

        # On vérifie qu'on a bien les droits
        if owner == login and imprimeur_not_required or is_imprimeur(request):
        # On est obligé de recréditer par l'intermediaire de www-data, l'user n'en a pas le droit
        # TODO : quand les impressions seront des factures, il suffira de detruire la facture correspondante au jid
            if crans_jid and prix:
                ldap = shortcuts.lc_ldap_admin()
                adherent = ldap.search(u'uid=%s' % owner, mode="w")[0]
                with adherent as adh:
                    adh.solde(prix, u"Impression ratee, jid=%s, tache=%s" % (crans_jid, task),login=login)
                    adh.save()
                jo.result = "Cancelled"
                jo.save()
                messages.success(request, _(u"La tâche %s a été annulée.") % (task))
        else:
            return "Denied"
    except:
        return "Introuvable"

@login_required
def delete_job(request, jid=-1):
    # Supprime les taches de la queue et recrédite
    crans_jid = None
    # On récupère le job
    try:
        jid = int(jid)
    except:
        pass

    # On récupère la file d'attente actualisée
    lpq_proc = subprocess.Popen(["lpq"], stdout=subprocess.PIPE)
    lpq_jobs = lpq_proc.stdout.readlines()[2:]

    # On garde selon le jid et on supprime
    lpq_jobs = [job.split() for job in lpq_jobs if int(job.split()[2]) == jid]
    if lpq_jobs == []:
        messages.error(request, _(u"Cette tâche n'est plus dans la file d'attente du serveur."))
    else:
        job = lpq_jobs[0]
        try:
            crans_jid = job[3].split(":")[0]
            result = make_recredit(request, crans_jid, imprimeur_not_required=True)
            if result == "Denied":
                messages.error(request, _(u"Vous n'êtes pas le propriétaire de cette tâche, vous ne pouvez pas l'annuler."))
                return HttpResponseRedirect("/impressions/gestion/")
            elif result == "Introuvable":
                messages.error(request, _(u"Tâche introuvable, impossible de recréditer"))
            subprocess.Popen(["cancel",str(jid)], stdout=subprocess.PIPE)
        except IndexError:
            messages.error(request, _(u"Impossible de recréditer la tâche"))

    return HttpResponseRedirect("/impressions/gestion/")

@login_required
def lost_job(request, jid=-1):
    # Permet aux imprimeurs de recréditer les taches paumées entre cups et l'imprimante
    # On récupère le job
    try:
        jid = int(jid)
    except:
        messages.error(request, _(u"Action impossible, jid introuvable"))
        return HttpResponseRedirect("/impressions/gestion/")

    result = make_recredit(request, jid, imprimeur_not_required=False)
    if result == "Denied":
        messages.error(request, _(u"Droits imprimeurs requis"))
        return HttpResponseRedirect("/impressions/gestion/")
    elif result == "Introuvable":
        messages.error(request, _(u"Tâche introuvable, impossible de recréditer"))
    return HttpResponseRedirect("/impressions/gestion/")


def erreur(request):
    return django.shortcuts.render_to_response("impressions/affichage.html")
