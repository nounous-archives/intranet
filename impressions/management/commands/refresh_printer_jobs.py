from django.core.management.base import BaseCommand, CommandError
from impressions.models import Jobs
from urllib2 import URLError

class Command(BaseCommand):
    help = 'Rafrachit les jobs de l\' imprimante'

    def handle(self, *args, **options):
        try:
            Jobs.objects.sync_jobs()
        except URLError:
            pass
