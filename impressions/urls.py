# -*- coding: utf-8 -*-
"""
    Contient les urls de l'application impression
"""

from django.conf.urls import url

import views

urlpatterns = [
    url(r'^$', views.view, name="view"),
    url(r'^erreurs/$', views.erreur, name="erreurs"),
    url(r'^gestion/$', views.gestion, name="gestion"),
    url(r'^gestion/delete/(?P<jid>[^/]*)/$', views.delete_job, name="delete"),
    url(r'^gestion/lostjob/(?P<jid>[^/]*)/$', views.lost_job, name="lostjob"),
    url(r'^imprimante/(?P<pk>\d+)/$', views.MainPrinterView.as_view(), name='printer'),
]
