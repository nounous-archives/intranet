# -*- coding: utf-8 -*-

from django import forms
from django.forms import BaseFormSet, formset_factory
from intranet import settings
from django.utils.translation import ugettext_lazy as _

import models

# Pour créer les dossier et faire des pdfinfo
import subprocess
import re
import os, os.path
import pwd

def get_storage_path(login, filename):
    """Donne le chemin vers le fichier, pour enregistrement"""
    if os.getenv('DBG_PRINTER', '0') == '0':
        cmd = ["sudo", "-n", "/usr/scripts/utils/chown_impressions.sh", login]
        path = subprocess.check_output(cmd).strip()
        return os.path.join(path, filename)
    else:
        curuser = pwd.getpwuid(os.getuid())[0]
        os.makedirs("/tmp/intranet-%s/%s" % (curuser, login))
        return "/tmp/intranet-%s/%s/%s" % (curuser, login, filename)

def pdfinfo(filepath):
    """Renvoi le résultat d'un pdfinfo sur un fichier"""
    filename = os.path.basename(filepath)
    # On commence par vérifier qu'on a bien affaire à un pdf
    fileinfo = subprocess.Popen(["mimetype", "-b", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    fileinfo = fileinfo.communicate()[0]
    is_pdf = "pdf" in ''.join(fileinfo).lower()
    if not is_pdf:
        return {}, is_pdf
    # Si on arrive ici, c'est qu'on a un document PDF. On récupère alors les informations nécéssaires pour traiter ce dernier.
    infos = subprocess.Popen(["pdfinfo", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    infos = infos.communicate()[0]
    infos = dict(re.findall('(.*?): *(.*)', infos))
    return infos, is_pdf

class UploadForm(forms.Form):
    """Formulaire d'envoi d'un fichier à imprimer"""
    fichier = forms.FileField(label=_(u"Fichier à imprimer"))
    javascript_function = "add();"
    fichier.widget.attrs["onChange"] = javascript_function
    fichier.widget.attrs["accept"] = "application/pdf"

    def clean(self): # TODO penser à supprimer le fichier si on foire
        """On vérifie qu'on nous a pas filé n'importe quoi."""
        out = forms.Form.clean(self)
        # On vérifie qu'un fichier est envoyé
        if out.has_key("fichier"):
            fichier = out["fichier"]
            filename = out["fichier"].name
            # On y enregistre ensuite le fichier
            login_used = self.real_user
            filepath = get_storage_path(login_used, filename)
            with open(filepath, 'wb+') as destination:
                for chunk in fichier.chunks():
                    destination.write(chunk)
            # On met tout ça dans une table
            job = models.Jobs.objects.create()
            job.login = self.login
            job.file = filename
            if self.club:
                job.club = self.club
            job.save()
            # On vérifie maintenant que le fichier à des caractéristiques acceptables
            if fichier != None and fichier.size > settings.MAX_PRINTFILE_SIZE:
                raise forms.ValidationError(_(u"Fichier trop volumineux, le maximum est de %(max) octets (actuellement %(size)s octets)") % {
                    'size': fichier.size,
                    'max': settings.MAX_PRINTFILE_SIZE,
                })
            infos, is_pdf = pdfinfo(filepath)
            if not is_pdf:
                raise forms.ValidationError( _(u"Votre fichier %s n'est pas un fichier PDF.") % (filename))
            try:
                pageno = int(infos["Pages"])
                out["pageno"] = pageno
            except (KeyError, ValueError) as e:
                raise forms.ValidationError(
                    _(u"Impossible de récupérer les informations sur le fichier PDF %(filename)s.") % {'filename' : filename}
                )
        else:
            print("toto")
            raise forms.ValidationError( u"Merci d'indiquer un fichier à imprimer")

        return out

class PrintForm(forms.Form):
    """Formulaire pour spécifier les paramètres d'impression"""
    couleur = forms.ChoiceField(label=_(u"Couleur ou N&B"),
                                choices=[('nb', _(u'Noir & Blanc')), ('couleur', _(u'Couleur'))],
                                initial='nb')
    disposition = forms.ChoiceField(label=_(u"Disposition"),
                                    choices=[('recto-verso', _(u'Recto/Verso')), ('recto', _(u'Recto')), ('livret', _(u'Livret'))],
                                    initial='recto-verso')
    format = forms.ChoiceField(label=_("Format du papier"),
                               choices=[('A4', u'A4'), ('A3', u'A3')],
                               initial='A4')
    agrafes = forms.ChoiceField(label=_("Agrafes"),
        choices=[
            ("none", _(u"Aucune")),
            ("hg", _(u"une en haut à gauche")),
            ("hd", _(u"une en haut à droite")),
#            ("bg", _(u"une en bas à gauche")), #TO TRANSLATE
#            ("bd", _(u"une en bas à droite")), #TO TRANSLATE
            ("g", _(u"deux sur le bord gauche")),
            ("d", _(u"deux sur le bord droit")),
        ],
        initial="none")
    # Les options commentées n'existent pas
    perforation = forms.ChoiceField(label="Perforation",
        choices=[
            ("None", _(u"Aucune")),
            ("2HolePunchLeft", _(u"2 trous à gauche")),
            ("2HolePunchRight", _(u"2 trous à droite")),
            ("2HolePunchTop", _(u"2 trous en haut")),
            ("2HolePunchBottom", _(u"2 trous en bas")),
#            ("3HolePunchLeft", _(u"3 trous à gauche")), #TO TRANSLATE
#            ("3HolePunchRight", _(u"3 trous à droite")), #TO TRANSLATE
#            ("3HolePunchTop", _(u"3 trous en haut")), #TO TRANSLATE
            ("4HolePunchLeft", _(u"4 trous à gauche")),
            ("4HolePunchRight", _(u"4 trous à droite")),
#            ("4HolePunchTop", _(u"4 trous en haut")), #TO TRANSLATE
        ],
        initial="None")
    copies = forms.IntegerField(label=_("Nombre de copies"), initial=1, min_value=1)
    filenom = forms.CharField(widget=forms.HiddenInput(), required=True)
    pageno = forms.IntegerField(widget=forms.HiddenInput(), required=True)
    # Pour mettre à jour le coût, on utilise javascript
    javascript_function = "javascript:update_prix_tot();"
    for champ in [couleur, disposition, format, agrafes, copies]:
        champ.widget.attrs["onChange"] = javascript_function
    del champ # Gruiiik
    copies.widget.attrs["onKeyUp"] = javascript_function

    def clean(self):
        """Certains valeurs des paramètres sont incompatibles."""
        out = forms.Form.clean(self)
        return out


class ClubForm(forms.Form):
    club_list = forms.ChoiceField(label=_(u'Imprimer pour'), required=False)

    def __init__(self, ldap_user, *args, **kwargs):
        super(ClubForm, self).__init__(*args, **kwargs)
        liste_compte = []
        for compte in [ldap_user] + ldap_user.clubs() + ldap_user.imprimeur_clubs():
            try:
                if compte['solde']:
                   solde = u" (" + unicode(compte['solde'][0]) + u"\u20AC)"
                else:
                   solde = u" (0\u20AC)"
                label = unicode(compte['nom'][0]) + solde
                uid = compte['uid'][0]
                if (uid, label) not in liste_compte:
                    liste_compte.append((uid, label))
            except KeyError:
                pass
        self.fields['club_list'].choices = tuple(liste_compte)

class UploadFormSet(BaseFormSet):
    def clean(self):
        """Vérifie que deux fichiers n'ont pas le même nom."""
        if any(self.errors):
            return
        noms = []
        if len(self.forms) ==  1 and not self.forms[0].cleaned_data:
            raise forms.ValidationError(_(u"Merci de donner au moins un fichier à imprimer"))
        for form in self.forms:
            if not form.cleaned_data:
                continue
            nom = form.cleaned_data['fichier'].name
            if nom in noms:
                raise forms.ValidationError(_(u"Les fichiers doivent avoir des noms différents."))
            noms.append(nom)
